#Frame

&ensp;&ensp;&ensp;&ensp;[Frame](http://git.oschina.net/simplething/frame/wikis/home)是以spring为核心的快速搭建项目工具。做过几个项目后，越来越觉的那些框架的问题所在，于是我决定自己搞一个。
我认为做任何事情都要简单到极致，我就是要做一个方便开发的框架，那名字也叫框架好了，于是命名为frame。
好在我百度后，发现还没有叫这个名字的项目。窃喜!


### frame的特性如下：

+ frame中99%都是ajax请求。frame做的工作就像DIY的电脑，将各种牛逼的开源工具装配起来。
+ frame完全依赖spring，包含spring-mvc,spring-jdbc。
+ frame用maven构建，git控制版本。
+ frame集成了shiro做安全,牛逼的Druid做数据库连接池。
+ frame本身会集成一套bootstrap风格的ui。
+ frame提供一个通用的DAO来满足所有的数据库操作。
+ frame不建议到处使用service层，一般情况controllor就可以解决大部分问题。
+ frame的字典缓冲是在浏览器端。
+ frame权限控制粒度为每个uri，可以实现按钮级的权限控制。
+ frame中可以很方便的管理sql,可以很方便的查看执行过的sql痕迹。
+ frame支持对象属性的服务器端验证，用的是注解哦，不用自己写代码搞！
+ frame自己带了一套系统控制功能，包含*用户角色，用户分组，用户管理，资源管理，字典管理，菜单管理，日志管理，frame介绍页面* 
  实现这些功能主要代码是一个controllor 300行代码带注释，2个shiro定制化实现类，
  11个领域对象:就是些描述角色，分组，用户等的javabean。12个jsp文件主要用来表示不同的功能页面。

需要注意：
log4jdbc 的依赖,中央仓库找不到了,请将项目下lib目录下的jar上传到本地仓库的第三方仓库
然后 修改项目的pom 中关于该jar的依赖 为你仓库中的坐标

## WARNING
请保证提交到版本库的java代码没有警告信息！！！！
讨论群加：84686585

### [开发计划](http://git.oschina.net/simplething/frame/wikis/TODO)