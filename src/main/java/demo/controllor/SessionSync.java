package demo.controllor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class SessionSync {
	
	@Autowired
	private CacheManager cacheManager;
	
	@RequestMapping(value = "/getSessionInCluster", method = RequestMethod.POST)
	@ResponseBody 
	public List<String> getSession(HttpServletRequest request){
		//打印当前系统中工作的cache 名称
		System.out.println(cacheManager.getCacheNames());
		Cache cache=cacheManager.getCache("defaultCache");
		ValueWrapper vw=cache.get("id");
		Object ss=request.getSession().getId();
		
		if(vw!=null){
			List<String> list=(List<String>)vw.get();
			if(!list.contains(ss)){
				list.add(ss.toString());
			}
			return list;
		}else{
			List<String> mm=new ArrayList<String>();
			mm.add(ss.toString());
			cache.put("id", mm);    
		     return mm;
		}
		
	}

}
