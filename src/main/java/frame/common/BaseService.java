package frame.common;

import java.io.Serializable;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import frame.dao.DAO;
/**
 * 基本的Service抽象类。
 * 提供一个logger对象给子类，共记录日志使用
 * 提供获取当前用户的方法。
 * @author MR.CUIPENG
 *
 */
public abstract class BaseService implements Serializable{
	
   private static final long serialVersionUID = 1641696170272423219L;
   
   @Autowired
   private HttpSession session;
   @Autowired
   protected DAO dao;
   /** Logger that is available to subclasses */
   protected final Log logger = LogFactory.getLog(getClass());

   public Object getUser() {
		return session.getAttribute("current_user");
   }
   
}
