package frame.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
/**
 * 配置文件管理类，管理系统中的所有properties配置文件信息
 * 好处是无缝的整合到了spring中，所有的配置信息可以在xml
 * 文件中和代码中很方便的得到。
 * @author 崔鹏
 *
 */
public class Config extends PropertyPlaceholderConfigurer {
	private static Map<String, Object> propertiesMap;

	@Override
	protected void processProperties(
			ConfigurableListableBeanFactory beanFactoryToProcess,
			Properties props) throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		propertiesMap = new HashMap<String, Object>();
		for (Object key : props.keySet()) {
			String keyStr = key.toString();
			String value = props.getProperty(keyStr);
			propertiesMap.put(keyStr, value);
		}
	}

	public static Object getContextProperty(String name) {
		return propertiesMap.get(name);
	}
}
