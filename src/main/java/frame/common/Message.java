package frame.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 消息类，定义了一个通用的消息形式。其中包含消息主体和消息类型(成功，错误，提示等);
 * @author MR.CUIPENG
 *
 */
public class Message {
	
	//操作成功
	public static Message OK=new Message("FSS0000");
	//操作失败
	public static Message ERR=new Message("FSE0000");
	//新增成功
	public static Message ADD_OK=new Message("FSS0001");
	//编辑成功
	public static Message EDIT_OK=new Message("FSS0002");
	//删除成功
	public static Message DEL_OK=new Message("FSS0003");
	//新增失败
	public static Message ADD_ERR=new Message("FSE0001");
	//编辑失败
	public static Message EDIT_ERR=new Message("FSE0002");
	//删除失败
	public static Message DEL_ERR=new Message("FSE0003");
	//消息内容
	private String msg;
	//替换者，按顺序替换消息中的占位符
	private List<Object> replacer=new ArrayList<Object>();

	public Message() {/*default constructor*/}
	
	public Message(String msg) {
		this.msg = msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public List<Object> getReplacer() {
		return replacer;
	}
	public void setReplacer(String replacer) {
		this.replacer.add(replacer);
	}
	public void setReplacer(Object[] replacer) {
		List<Object> rp=Arrays.asList(replacer);
		int len=rp.size();
		if(len>1){
			this.replacer.addAll(rp.subList(1,len));
		}
	}
	@Override
	public String toString() {
		return "{\"msg\":\"" + msg +  "\", \"replacer\":\""+replacer+"\"}";
	}
}
