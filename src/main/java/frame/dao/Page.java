package frame.dao;

import java.util.List;

import frame.common.BaseBean;

/**
 * 分页的页接口对象。用来描述分页显示中的页对象。
 * 包含了当前页数据。以及总页数。
 * @author MR.CUIPENG
 *
 */
public class Page<T> extends BaseBean{
	
	private static final long serialVersionUID = -5520795328623807804L;
	
	private List<T> data;
	private int total;
	
	public Page(List<T> data,int total) {
		this.data=data;
		this.total=total;
	}
	/**
	 * 获取分页数据
	 * @return
	 */
	public List<T> getData(){
		return data;
	}
	/**
	 * 获取总页数
	 * @return
	 */
	public int getTotal(){
		return total;
	}

}
