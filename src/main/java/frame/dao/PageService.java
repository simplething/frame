package frame.dao;

/**
 * 提供分页服务。已经被弃用。分页方法移到了DAO接口中。
 * @author MR.CUIPENG
 *
 */
@Deprecated
public interface PageService{
	/**
	 * 查询分页信息，该功能能够根据sql和args参数的摘要信息缓存总页数，如果是相同的摘要信息，则不执行
	 * 总页数查询语句。
	 * @param pageInfo 分页对象中包含的对象的类信息。
	 * @param sql 数据库查询语句
	 * @param pageStart 第几页
	 * @param pageSize 每页多少条记录
	 * @param args 查询条件
	 * @return 分页好的分页信息
	 */
	public <T> Page<T> paginate(Class<T> pageInfo,String sql,int pageStart,int pageSize,Object... args);

}
