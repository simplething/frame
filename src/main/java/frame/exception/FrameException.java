package frame.exception;

import java.util.Arrays;

/**
 * Frame异常类,方便aop层做统一的异常处理。设计为包含了最初的请求参数，在打印异常信息的时候，对应
 * 参数信息也可由被打印出来。
 * 常见异常类型自动提示。比如数据入库主键冲突，提示为 (主键,...)+已经存在，插入失败!
 * 对应的消息编号为 SYS0010，需要返回主键数据。
 * @author MR.CUIPENG
 *
 */
public class FrameException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	//将当前请求的参数包含到异常类中，方便问题调试
	private Object[] arguments;
	public FrameException() {
		super();
	}
	public FrameException(String msg) {
		super(msg);
	}
	public FrameException(String message, Throwable cause) {
        super(message, cause);
    }
	public FrameException(Throwable cause) {
        super(cause);
    }
	
	public Object[] getArguments() {
		return arguments;
	}
	
	public void setArguments(Object[] arguments) {
		this.arguments = arguments;
	}
	
	@Override
	public String toString() {
		return "FrameException:"+this.getMessage()+" [arguments=" + Arrays.toString(arguments) + "]";
	}
	
	
	
}
