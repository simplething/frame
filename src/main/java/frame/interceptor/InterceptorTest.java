package frame.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

//这个类当前只是用来实验SPRING mvc中的拦截器的用法
public class InterceptorTest implements HandlerInterceptor {

	public boolean preHandle(HttpServletRequest request,  
	        HttpServletResponse response, Object handler) throws Exception {  
	    return true;  
	}  
	  
	@Override  
	public void postHandle(HttpServletRequest request,  
	        HttpServletResponse response, Object handler,  
	        ModelAndView modelAndView) throws Exception {  
	    System.out.println("------------------Post-handle");  
	}  
	  
	@Override  
	public void afterCompletion(HttpServletRequest request,  
	        HttpServletResponse response, Object handler, Exception ex)  
	        throws Exception {  
	    System.out.println("------------------After completion handle");  
	}
	

}
