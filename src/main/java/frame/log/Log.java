package frame.log;

import java.util.Arrays;

import frame.security.entity.User;

/**
 * 记录用户的操作日志。包含：当前用户，请求参数，调用方法，执行结果。
 * 对应的是系统操作日志。
 * @author MR.CUIPENG
 *
 */
public class Log{
	
	private User user;
	private Object[] arguments;
	private Object result;
	
	public Log() {
		super();
	}

	public Log(User user, Object[] arguments, Object result) {
		super();
		this.user = user;
		this.arguments = arguments;
		this.result = result;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Object[] getArguments() {
		return arguments;
	}

	public void setArguments(Object[] arguments) {
		this.arguments = arguments;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "Log [user=" + user + ", arguments="
				+ Arrays.toString(arguments) + ", result=" + result + "]";
	}
	
}
