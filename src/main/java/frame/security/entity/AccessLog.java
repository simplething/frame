package frame.security.entity;


import java.sql.Timestamp;
import frame.common.BaseBean;
/**
 * 因为该日志中包含的信息和log4j要记录的日志内容差别较大。故该日志不通过log4j来入库。
 * @author 崔鹏
 *
 */
public class AccessLog extends BaseBean{
	
	private static final long serialVersionUID = -6168335103043460213L;
	
	//private String seq;//序列号
	private String url;//请求路径
	private String user_id;//用户ID
	private String user_name;//用户名称
	private String paramin;//请求参数
	private String descp;//描述 ，用于记录额外的关于该请求执行的结果，比如报错信息或是其他
	private Timestamp create_time;//入库时间，记录时间
	private Timestamp start;//查询开始时间
	private Timestamp end;//查询结束时间
	
	public void setStart(Timestamp start) {
		this.start = start;
	}
	public void setEnd(Timestamp end) {
		this.end = end;
	}
	public Timestamp getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Timestamp createTime) {
		create_time = createTime;
	}
	public AccessLog() {
		this.tableName="sys_log";
	}
	
	protected String appender(){
		if(start!=null&&end==null){
			args.add(start);
			return "create_time>=?";
		}
		if(start==null&&end!=null){
			args.add(end);
			return "create_time<=?";	
		}
		if(start!=null&&end!=null){
			args.add(start);
			args.add(end);
			return "create_time between ? and ?";
		}
		return super.appender();
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String userId) {
		user_id = userId;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String userName) {
		user_name = userName;
	}
	public String getParamin() {
		return paramin;
	}
	public void setParamin(String paramin) {
		this.paramin = paramin;
	}
	public String getDescp() {
		return descp;
	}
	public void setDescp(String descp) {
		this.descp = descp;
	}

}

