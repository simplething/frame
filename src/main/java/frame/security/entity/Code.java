package frame.security.entity;

import frame.common.BaseBean;
/**
 * 系统字典表对应的实体对象
 * @author 崔鹏
 *
 */
public class Code extends BaseBean{
	
	private static final long serialVersionUID = 7531062126515207558L;
	private String did;//代码
	private String dname;//名称
	private String dtid;//字典类型ID
	private String pdid;//父即字典ID
	
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getDtid() {
		return dtid;
	}
	public void setDtid(String dtid) {
		this.dtid = dtid;
	}
	public String getPdid() {
		return pdid;
	}
	public void setPdid(String pdid) {
		this.pdid = pdid;
	}
}
