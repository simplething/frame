package frame.security.entity;

import frame.common.BaseBean;
/**
 * 系统字典类型表
 * @author 崔鹏
 *
 */
public class CodeType extends BaseBean{
	
	private static final long serialVersionUID = 4903034313421006374L;
	
	private String dtid;//字典类型ID
	private String dtname;//字典类型名称
	private String remark;//备注
	
	public String getDtid() {
		return dtid;
	}
	public void setDtid(String dtid) {
		this.dtid = dtid;
	}
	public String getDtname() {
		return dtname;
	}
	public void setDtname(String dtname) {
		this.dtname = dtname;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
