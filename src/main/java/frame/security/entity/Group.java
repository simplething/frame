package frame.security.entity;

import org.hibernate.validator.constraints.NotEmpty;

import frame.common.BaseBean;

public class Group extends BaseBean{

	private static final long serialVersionUID = 3348773814862537626L;
	private int group_id;
	@NotEmpty
	private String group_name;
	@NotEmpty
	private String valid_flag;// Y/N
	
	
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	public String getValid_flag() {
		return valid_flag==null||"N".equals(valid_flag)?"N":"Y";
	}
	public void setValid_flag(String valid_flag) {
		this.valid_flag = valid_flag;
	}
	
}
