package frame.security.entity;

import frame.common.BaseBean;

public class GroupPermit extends BaseBean{
	
	private static final long serialVersionUID = 8910570119770646668L;
	private int group_id;
	private String group_name;
	private String permit;// Y/N
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public String getGroup_name() {
		return group_name;
	}
	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}
	public String getPermit() {
		return permit;
	}
	public void setPermit(String permit) {
		this.permit = permit;
	}

	
}
