package frame.security.entity;

import org.springframework.util.StringUtils;

import frame.common.BaseBean;

public class Menu extends BaseBean{

	private static final long serialVersionUID = 4171751648030330724L;
	private String menu_id;//菜单ID
	private String menu_name;//菜单名称
	private String parent_menu_id;//父级菜单ID
	private String pmenu_name;//父级菜单名称
	private String menu_order;//菜单顺序
	private String uri;//菜单指向的资源
	
	protected String appender(){
		return "menu_id<>'root'";
	}
	
	
	
	public   String select(){
		//作为抽象的跟节点，root不查询到前台
		StringBuilder sql=new StringBuilder("select a.menu_id,a.menu_name,(select b.menu_name from sys_menu b where b.menu_id=a.parent_menu_id) as pmenu_name," +
				"menu_order,uri  from sys_menu a where a.menu_id<>-1");
		if(!StringUtils.isEmpty(uri)){
			sql.append(" and uri like '%").append(uri).append("%'");
		}
        if(!StringUtils.isEmpty(menu_name)){
        	sql.append(" and menu_name like '%").append(menu_name).append("%'");
		}
		return sql.toString();
	}
	
	public String getMenu_id() {
		return menu_id;
	}
	public void setMenu_id(String menuId) {
		menu_id = menuId;
	}
	public String getMenu_name() {
		return menu_name;
	}
	public void setMenu_name(String menuName) {
		menu_name = menuName;
	}
	public String getParent_menu_id() {
		return parent_menu_id;
	}
	public void setParent_menu_id(String parentMenuId) {
		parent_menu_id = parentMenuId;
	}
	public String getMenu_order() {
		return menu_order;
	}
	public void setMenu_order(String menuOrder) {
		menu_order = menuOrder;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}


	public String getPmenu_name() {
		return pmenu_name;
	}

	public void setPmenu_name(String pmenuName) {
		pmenu_name = pmenuName;
	}
	

}
