package frame.security.entity;

import java.util.List;

import frame.common.BaseBean;

public class Permisson extends BaseBean{

	private static final long serialVersionUID = 2852453017285865707L;
	private String uri;
	private List<RolePermit> roles;
	private List<GroupPermit> groups;
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public List<RolePermit> getRoles() {
		return roles;
	}
	public void setRoles(List<RolePermit> roles) {
		this.roles = roles;
	}
	public List<GroupPermit> getGroups() {
		return groups;
	}
	public void setGroups(List<GroupPermit> groups) {
		this.groups = groups;
	}
	
	
}
