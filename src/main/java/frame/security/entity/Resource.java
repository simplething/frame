package frame.security.entity;

import org.hibernate.validator.constraints.NotEmpty;

import frame.common.BaseBean;

public class Resource extends BaseBean{
	
	private static final long serialVersionUID = 7586940295903981622L;
	
	@NotEmpty
	private String uri;
	@NotEmpty
	private String f_name;
	@NotEmpty
	private String valid_flag;
	private String description;
	
	public Resource() {
	   this.tableName="sys_resource";
	}
	
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getValid_flag() {
		return valid_flag;
	}
	public void setValid_flag(String valid_flag) {
		this.valid_flag=valid_flag==null||"N".equals(valid_flag)?"N":"Y";
	}

}
