package frame.security.entity;

import org.hibernate.validator.constraints.NotEmpty;

import frame.common.BaseBean;

public class Role extends BaseBean{
	
	private static final long serialVersionUID = 5476365458977142139L;
	
	private int role_id;
	@NotEmpty
	private String role_name;
	@NotEmpty
	private String valid_flag;
	
	
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public String getRole_name() {
		return role_name;
	}
	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}
	public String getValid_flag() {
		return valid_flag==null||"N".equals(valid_flag)?"N":"Y";
	}
	public void setValid_flag(String valid_flag) {
		this.valid_flag = valid_flag;
	}
	
}
