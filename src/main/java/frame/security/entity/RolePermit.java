package frame.security.entity;

import frame.common.BaseBean;

public class RolePermit extends BaseBean{
	
	private static final long serialVersionUID = -4596478574180827739L;
	private int role_id;
	private String role_name;
	private String permit;//是或否
	public int getRole_id() {
		return role_id;
	}
	public void setRole_id(int role_id) {
		this.role_id = role_id;
	}
	public String getRole_name() {
		return role_name;
	}
	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}
	public String getPermit() {
		return permit;
	}
	public void setPermit(String permit) {
		this.permit = permit;
	}

}
