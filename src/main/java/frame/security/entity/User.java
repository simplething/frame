package frame.security.entity;

import java.sql.Date;
import java.util.List;

import frame.common.BaseBean;


public class User extends BaseBean{

	private static final long serialVersionUID = 1338259184235034276L;
	
	private String user_id;
	private String user_name;
	private String password;
	private String user_type;
	private Date create_date;
	
	private List<RolePermit> roles;
	private List<GroupPermit> groups;
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public List<RolePermit> getRoles() {
		return roles;
	}
	public void setRoles(List<RolePermit> roles) {
		this.roles = roles;
	}
	public List<GroupPermit> getGroups() {
		return groups;
	}
	public void setGroups(List<GroupPermit> groups) {
		this.groups = groups;
	}
	public String getUser_type() {
		return user_type;
	}
	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}
	
}

