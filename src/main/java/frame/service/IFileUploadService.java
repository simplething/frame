package frame.service;

import java.io.File;

/**
 * 文件上传服务，由控制器frame.utils.FileUpload.java统一进行调度。
 * 用来补充单一的上传Controller无法 满足多样的文件上传服务中的信息入库操作。
 * 所有上传文件后需要入库的功能都需要实现该接口。
 * @author 崔鹏 20140106
 *
 */
public interface IFileUploadService {
	/**
	 * 文件信息保存入库操作在这个方法的实现中显示。
	 * @param file 上传到服务器的文件，单个文件或多个文件都支持
	 */
	public void updateFileInfo(File... file);

}
