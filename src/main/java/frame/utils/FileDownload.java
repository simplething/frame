package frame.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import frame.common.Config;
import frame.exception.FrameException;

/**
 * frame框架提供的文件下载工具。可以服务器上任意有权限位置的文件。
 * @author cuip
 *
 */
@Controller
@RequestMapping("/download")
public class FileDownload {
	
	/**
	 * 设置下载文件时的缓存大小
	 */
	private int buffsize=2048;
	
	/**
	 * 下载一个文件，一般的如果文件在工程的root目录下，则不需要这个方法。通过url直接打开即可。
	 * 这个方法用来对应无法通过url直接下载的文件
	 * @param place  路径，一般通过配置文件指定，与上传服务使用相同的配置
	 * @param fileName 文件名称
	 */
	@RequestMapping(value = "/{place}/file/{fileName}")
	@ResponseBody
	public void getFile(@PathVariable String place,@PathVariable String fileName, HttpServletRequest request,HttpServletResponse response){
		//1.设置文件ContentType类型，这样设置，会自动判断下载文件类型  
        response.setContentType("multipart/form-data");  
        //2.设置文件头：最后一个参数是设置下载文件名(假如我们叫a.pdf) 
        //new String(realName.getBytes("utf-8"), "ISO8859-1")
        response.setHeader("Content-Disposition", "attachment;fileName="+fileName);  
        String fileDir = (String) Config.getContextProperty(place);// 获取配置的上传文件保存路径
      //获取保存文件可使用的路径
		String path = FrameUtils.getPath(request, fileDir);
      //通过文件路径获得File对象(假如此路径中有一个download.pdf文件) 
        if(!path.endsWith(File.separator)){
        	path=path+File.separator;
        }
        File file = new File(path + fileName);  
        //设置文件大小
        response.setHeader("Content-Length", String.valueOf(file.length())); 
        
        BufferedInputStream bis = null;  
        BufferedOutputStream bos = null; 
        try {  
        	bis = new BufferedInputStream(new FileInputStream(file)); 
            bos = new BufferedOutputStream(response.getOutputStream());    
            byte[] buff = new byte[buffsize];    
            int bytesRead;    
            while (-1 != (bytesRead = bis.read(buff, 0, buffsize))) {    
                bos.write(buff, 0, bytesRead);    
            }    
            bis.close();    
            bos.close();  
        } catch (IOException e) {  
            throw new FrameException("下载文件异常");
        }  
	}

}
