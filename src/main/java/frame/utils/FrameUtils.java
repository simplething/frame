package frame.utils;

import java.io.File;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.PropertyAccessor;

/**
 * frame中用的一些工具方法。一般是静态的。
 * @author MR.CUIPENG
 *
 */
public class FrameUtils {
	
	/**
	 * 如果是绝对路径 包含:和以文件系统 分隔符开头的字符串都算是绝对路径
	 * 处理字符串 fileDir，如果是绝对路径，如果是相对路径则返回基于应用所在位置的绝对路径
	 * @param request  请求对象
	 * @param fileDir  文件路径
	 * @return
	 */
	public static String getPath(HttpServletRequest request, String fileDir) {
		if(fileDir.contains(":")||fileDir.indexOf(File.separator)==0){
			return fileDir;
		}else{
		    return request.getSession().getServletContext().getRealPath(
					fileDir);// 上传的目录
		}
	}
	
	/**
	 * 将simple bean 中的属性按照order的顺序进行排列并返回。
	 * @param data simple bean 对象
	 * @param order 要求的属性排列顺序。
	 * @return 依照order顺序返回的bean属性值数组对象。
	 */
    public static Object[] beanProperties(Object data,String... order){
    	int len = order.length;
		Object[] args = new Object[len];
		PropertyAccessor pa=new BeanWrapperImpl(data);
		for(int i=0; i<len; i++){
			args[i] = pa.getPropertyValue(order[i]);
		}
    	return args;
    }
    /**
     * 将map对象中按照order顺序对应的key的value值以数组的形式返回
     * @param data map对象
     * @param order 要求的key排列顺序。
     * @return 依照order顺序返回的map中key值数组对象。
     */
    public static Object[] mapValues(Map<String,?> data,String... order){
    	int len = order.length;
		Object[] args = new Object[len];
		for(int i=0; i<len; i++){
			args[i] = data.get(order[i]);
		}
    	return args;
    }
	
	/**
	 * 获取String的MD5摘要信息
	 * @param source 源字符串
	 * @return 摘要信息
	 */
	public static final String encryptMD5(String source) {
		return new Md5Hash(source==null?"":source).toString();
	}
	/**
	 * 获取String的MD5摘要信息，并参合一个salt字符串来混合运算
	 * @param source 源字符串
	 * @param salt 参合字符串
	 * @return 信息摘要结果
	 */
	public static final String encryptMD5(String source,Object salt) {
		return new Md5Hash(source==null?"":source,salt).toString();
	}
	/**
	 * 获得bean的string描述
	 * @param obj 要分析的bean
	 * @return 字符串描述
	 */
	public static String toString(Object obj) {
		Map<String,String> info=new HashMap<String,String>();
		return getStringInfo(obj,info)+info.toString();
	}
	
	@SuppressWarnings("rawtypes")
	private static String getStringInfo(Object obj,Map<String,String> info){
		List<Object> visited = new ArrayList<Object>();
		if (obj == null)
			return "null";
		if (visited.contains(obj))
			return "....";
		visited.add(obj);
		
	
		Class cl = obj.getClass();
		if (cl == String.class)
			return (String) obj;
		if (cl.isArray()) {
			String r = "[";
			for (int i = 0; i < Array.getLength(obj); i++) {
				if (i > 0)
					r += ",";
				Object val = Array.get(obj, i);
				if (cl.getComponentType().isPrimitive())
					r += val;
				else
					r += getStringInfo(val,new HashMap<String, String>());
			}
			return r + "]";
		}
		
		if(cl.equals(Logger.class)||cl.equals(LinkedList.class)){//日志标记为删除
			return obj.toString();
		}
		
		if(cl.getSuperclass().equals(AbstractList.class)){
			
			return obj.toString();
		}
		System.out.println(cl.getName());
		System.out.println(cl.getSuperclass().getName());
		String objName = cl.getName();
		
		
		
		//以上是当前对象
		do {
			Field[] fields = cl.getDeclaredFields();
			/**
			 * Shield Java Access Control
			 */
			AccessibleObject.setAccessible(fields, true);
			for (Field f : fields) {
				/**
				 * Judge the field's modifier, whether it is a static or protected ||!Modifier.isProtected(f.getModifiers())
				 */
				if (!Modifier.isStatic(f.getModifiers())) {
					 try {
							Class t = f.getType();
							Object val = f.get(obj);
							/**
							 * Judge if field is a primitive type or Logger type
							 */
							if (t.isPrimitive()){
							  info.put(f.getName(), val.toString());
							}else if (!t.equals(Logger.class)){
							  info.put(f.getName(), getStringInfo(val,new HashMap<String, String>()));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
			}
			
			cl = cl.getSuperclass();
		} while (cl != null);
		
		return objName;
	}
}
