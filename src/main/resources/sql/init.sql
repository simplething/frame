---创建frame需要的数据库表结构 及数据
---the dababase name is frame but others are always ok
--角色表
drop table if exists sys_role;


--sys_role                                

create table sys_role
(
   role_id              int(4) not null auto_increment comment'角色代码信息',
   role_name            VARCHAR(32)
                         not null comment '角色名称信息',
   valid_flag           varchar(1) comment '有效标记：Y：表示有效；N：表示无效',
   primary key (role_id)
);

alter table sys_role comment '角色信息表';

--分组表
drop table if exists sys_group;


--sys_group                               

create table sys_group
(
   group_id             int(4) not null auto_increment comment '组代码信息',
   group_name           VARCHAR(32)
                         not null comment '组名称信息',
   valid_flag           varchar(1) comment '有效标记：Y：表示有效；N：表示无效',
   primary key (group_id)
);

alter table sys_group comment '用户组信息表';

--资源表
drop table if exists sys_resource;


--sys_resource                            

create table sys_resource
(
   uri                  varchar(128) not null comment '不带参数的请求路径能唯一指向系统中的一个功能或页面资源或是其他资源。',
   f_name               varchar(64),
   valid_flag           varchar(1) comment '有效标记：Y：表示有效；N：表示无效',
   description          varchar(1024)
);

alter table sys_resource comment '对系统资源进行描述的数据表';

--权限控制表
drop table if exists sys_authority;


--sys_authority                           

create table sys_authority
(
   SEQ                  bigint not null auto_increment,
   uri                  varchar(256) not null comment '不带参数的请求路径能唯一指向系统中的一个功能或页面资源或是其他资源。',
   rg_id                int(4) not null comment '组代码信息 or 角色代码信息',
   rg                   varchar(1) not null comment 'R or G 分别代表角色或者分组',
   primary key (SEQ)
);

alter table sys_authority comment '标识系统中的每个功能位置，通过url来完成，且指定每个功能对应的用户组和角色';

--用户表
drop table if exists user;

--user                                    

create table user
(
   user_id              VARCHAR(32)
                         not null comment '用户代码信息',
   user_name            VARCHAR(128)
                         not null comment '用户名称，即用户姓名',
   create_date          date comment '创建数据时间',
   password             varchar(64),
   primary key (user_id)
);

alter table user comment '用户信息表';

--用户分组关系表
drop table if exists sys_user_group;


--sys_user_group                          

create table sys_user_group
(
   user_id              VARCHAR(32) not null comment '角色名称信息',
   group_id             int(4) not null comment '角色代码信息',
   primary key (user_id, group_id)
);

alter table sys_user_group comment '用户与用户组关联信息表';

--用户角色关系表
drop table if exists sys_user_role;


--sys_user_role                           

create table sys_user_role
(
   user_id              VARCHAR(32) not null  comment '角色名称信息',                  
   role_id              int(4)  not null comment '角色代码信息',
   primary key (user_id, role_id)
);

alter table sys_user_role comment '用户角色关联信息表';

--菜单表
alter table sys_menu
   drop primary key;

drop table if exists sys_menu;

-- sys_menu

create table sys_menu
(
   menu_id              bigint not null auto_increment comment '菜单代码信息',
   menu_name            VARCHAR(128)
                         not null comment '菜单名称，通常与程序名称相同',
   parent_menu_id       bigint comment '菜单父亲代码',
   menu_order           VARCHAR(2)
                         comment '同级菜单下排序',
   uri                  varchar(256) comment '不带参数的请求路径能唯一指向系统中的一个功能或页面资源或是其他资源。',
  primary key (menu_id)
);

alter table sys_menu comment '菜单信息表，是树形结构，与程序对应';

--系统日志表
drop table if exists sys_log;


-- sys_log 

create table sys_log
(
   SEQ                  bigint not null auto_increment,
   URL                  varchar(1024),
   USER_ID              varchar(32),
   USER_NAME            varchar(128),
   PARAMIN              varchar(1024) comment '请求参数字符串描述，参数间用半角分号隔开',
   DESCP                varchar(512) comment '用于记录额外的关于该请求执行的结果，比如报错信息或是其他',
   CREATE_TIME          timestamp,
   primary key (SEQ)
);

alter table sys_log comment 'sys_log系统操作日志信息表';




--frame初始化数据

INSERT INTO user (`user_id`,`user_name`,`create_date`,`password`) VALUES ('admin','管理员','2013-09-27','admin');
INSERT INTO user (`user_id`,`user_name`,`create_date`,`password`) VALUES ('test','test','2014-07-31','s');


INSERT INTO sys_role (`role_id`,`role_name`,`valid_flag`) VALUES (1,'管理员','Y');
INSERT INTO sys_role (`role_id`,`role_name`,`valid_flag`) VALUES (2,'普通用户','Y');


INSERT INTO sys_group (`group_id`,`group_name`,`valid_flag`) VALUES (1,'系统','Y');
INSERT INTO sys_group (`group_id`,`group_name`,`valid_flag`) VALUES (2,'业务','Y');

INSERT INTO sys_user_role (`user_id`,`role_id`) VALUES ('admin',1);
INSERT INTO sys_user_group (`user_id`,`group_id`) VALUES ('admin',1);

INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('frame/mgr/resource.jsp','资源管理页面','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('mgr/resources.do','获取资源信息','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('mgr/addResources.do','新增资源','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('mgr/editResources.do','编辑资源','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('mgr/delResources.do','删除资源','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('frame/mgr/rolePermit.jsp','将资源授权到角色页面','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('mgr/getPermitRoles.do','获取资源角色授权关系','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('mgr/updatePermitRoles.do','更新资源角色授权关系','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('frame/mgr/groupPermit.jsp','将资源授权到分组页面','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('mgr/getPermitGroups.do','获取资源分组授权关系','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('mgr/updatePermitGroups.do','更新资源分组授权关系','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('frame/mgr/mgr.jsp','后台管理主页面','Y','系统必须有的初始化数据');
INSERT INTO sys_resource (`uri`,`f_name`,`valid_flag`,`description`) VALUES ('frame/mgr/role.jsp','角色页面','Y','');

INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (1,'frame/mgr/resource.jsp',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (4,'mgr/updatePermitRoles.do',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (6,'mgr/delResources.do',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (7,'frame/mgr/resource.jsp',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (10,'mgr/updatePermitRoles.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (12,'mgr/delResources.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (18,'mgr/resources.do',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (19,'mgr/resources.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (20,'mgr/addResources.do',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (21,'mgr/addResources.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (22,'mgr/editResources.do',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (23,'mgr/editResources.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (25,'mgr/updatePermitGroups.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (26,'frame/mgr/rolePermit.jsp',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (27,'mgr/getPermitRoles.do',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (28,'frame/mgr/groupPermit.jsp',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (29,'frame/mgr/groupPermit.jsp',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (30,'mgr/getPermitGroups.do',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (31,'mgr/getPermitGroups.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (35,'mgr/updatePermitGroups.do',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (36,'frame/mgr/mgr.jsp',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (37,'frame/mgr/mgr.jsp',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (38,'frame/mgr/rolePermit.jsp',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (39,'mgr/getPermitGroups.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (40,'mgr/getPermitRoles.do',1,'G');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (41,'frame/mgr/role.jsp',1,'R');
INSERT INTO sys_authority (`SEQ`,`uri`,`RG_id`,`rg`) VALUES (42,'frame/mgr/role.jsp',1,'G');

INSERT INTO sys_code_type (`DTID`,`DTNAME`,`REMARK`) VALUES ('yn','是否','翻译是或否');
INSERT INTO sys_code (`DID`,`DNAME`,`DTID`,`PDID`) VALUES ('N','否','yn','');
INSERT INTO sys_code (`DID`,`DNAME`,`DTID`,`PDID`) VALUES ('Y','是','yn','');

INSERT INTO sys_menu (`menu_id`,`menu_name`,`parent_menu_id`,`menu_order`,`uri`) VALUES (-1,'ROOT',NULL,NULL,NULL);
INSERT INTO sys_menu (`menu_id`,`menu_name`,`parent_menu_id`,`menu_order`,`uri`) VALUES (1,'ds',-1,'1','11');
INSERT INTO sys_menu (`menu_id`,`menu_name`,`parent_menu_id`,`menu_order`,`uri`) VALUES (3,'adfs',-1,'1','asdf');
