--frame中放置静态sql语句的地方。该文件会在frame启动的时候被SqlLoader这个对象进行解析。
--解析的结果会被保存在SQL_CACHE这个缓冲中。保存的形式是：key-value。key是@符号后面的内容
--value出现@后不带--这个符号的下一行的内容。所以sql文件的格式必须是 有--@打头的sql名称，独占 一行的sql语句内容。
--系统使用的sql语句，主要包含权限控制，菜单项载入。
------------------------------------
--@checkUserID  
--通过用户id,密码查询是否存在这个用户
------------------------------------
select count(1) from user where user_id=?
------------------------------------
--@insertUser  
--新增一条用户记录，主要为用户的认证信息
------------------------------------
insert into user (user_id,user_name,password,create_date) values(?,?,?,CURDATE());
------------------------------------
--@login  
--通过用户id,密码查询是否存在,并关联该用户的角色和分组信息。
-- 对于frame 如果用户角色为管理员，且属于系统组则判定为管理员，
-- 其他的都判定为普通用户
------------------------------------
select u.*,case when r.role_id=1 and g.group_id=1 then 'M' else 'C' end as user_type   from user u 
join sys_user_role r on u.user_id=r.user_id
join sys_user_group g on u.user_id=g.user_id
where u.password=? and u.user_id=?
------------------------------------
--@permission
-- 通过用户id，当前请求uri查询该用户是否有对该uri的访问权限
------------------------------------
select distinct a.uri from sys_user_group g 
join sys_user_role r 
on g.user_id=r.user_id
join sys_authority a
on a.rg_id=g.group_id and a.rg='G'
join sys_authority b
on b.rg_id=r.role_id and b.rg='R'
join sys_role s on r.role_id=s.role_id
join sys_group c on c.group_id=g.group_id
where s.valid_flag='Y' and c.valid_flag='Y' and r.user_id=?
------------------------------------
--@permitRoles
-- 查询系统中所有角色的授权状态
------------------------------------
select r.role_id,r.role_name,case when a.uri is null then 'N' else 'Y' end  as permit
from sys_role r left join sys_authority a on r.role_id=a.rg_id and a.rg='R' and a.uri=?
where r.valid_flag='Y'
------------------------------------
--@permitGroups
-- 查询系统中所有分组的授权状态
------------------------------------
select g.group_id,g.group_name,case when a.uri is null then 'N' else 'Y' end  as permit
from sys_group g left join sys_authority a on g.group_id=a.rg_id and a.rg='G' and a.uri=?
where g.valid_flag='Y' 
------------------------------------
--@addPermits
-- 查询系统中所有角色的授权状态
------------------------------------
insert into sys_authority(uri,rg_id,rg) values(?,?,?)
------------------------------------
--@delPermits
-- 查询系统中所有角色的授权状态
--rg = R or G
------------------------------------
delete from sys_authority where uri=? and rg_id=? and rg=?
------------------------------------
--@roles
-- 查询系统中所有的角色
------------------------------------
select role_id,role_name,valid_flag from sys_role
------------------------------------
--@addRole
-- 新增一个角色
------------------------------------
insert into sys_role(role_name,valid_flag) values(?,?)
------------------------------------
--@editRole
--编辑一个角色
------------------------------------
update sys_role set role_name=?,valid_flag=? where role_id=?
------------------------------------
--@delRole
-- 删除一个角色
------------------------------------
delete from sys_role where role_id=?
------------------------------------
--@delURole
-- 删除一个用户-角色关系，通过角色ID
------------------------------------
delete from sys_user_role where role_id=?
------------------------------------
--@delARole
-- 删除一个通过角色ID删除权限分配表中的对应的记录,通过对应的角色ID
------------------------------------
delete from sys_authority where rg='R' and rg_id=? 
------------------------------------
--@delAGroup
-- 删除一个通过分组ID删除权限分配表中的对应的记录,通过对应的分组ID
------------------------------------
delete from sys_authority where rg='G' and rg_id=? 
-----------------------------------
--@groups
-- 查询所有的分组信息
------------------------------------
select group_id,group_name,valid_flag from sys_group
-----------------------------------
--@addGroup
-- 新增一个分组
------------------------------------
insert into sys_group(group_name,valid_flag) values(?,?)
-----------------------------------
--@eidtGroup
-- 编辑一个分组
------------------------------------
update sys_group set group_name=?,valid_flag=? where group_id=?
-----------------------------------
--@delGroup
-- 删除一个分组,通过分组ID
------------------------------------
delete from sys_group where group_id=?
------------------------------------
--@delUGroup
-- 删除一个用户-分组关系,通过分组ID
------------------------------------
delete from sys_user_group where group_id=?
------------------------------------
-----@resources  
-- 查询系统中的资源信息  这个查询语句已经不用了
------------------------------------
--select uri,f_name,description from sys_resource

------------------------------------
--@addResources
-- 新增系统资源
------------------------------------
insert into sys_resource(uri,f_name,valid_flag,description) values(?,?,?,?)
------------------------------------
--@editResources
-- 编辑一个系统资源
------------------------------------
update sys_resource set f_name=?,valid_flag=?,description=? where uri=?
------------------------------------
--@delResources
--删除系统资源
------------------------------------
delete from sys_resource where uri=?
------------------------------------
--@delAResource
-- 删除一个通过资源uri删除权限分配表中的对应的记录
------------------------------------
delete from sys_authority where uri=? 
------------------------------------
--@users
-- 查询系统中所有的用户
------------------------------------
select * from user
------------------------------------
--@addUser
-- 新增一个用户
------------------------------------
insert into user (user_id,user_name,create_date,password) values (?,?,CURDATE(),?)
------------------------------------
--@editUser
--编辑一个用户
------------------------------------
update user set user_name=?,password=? where user_id=?
------------------------------------
--@delUser
-- 删除一个用户
------------------------------------
delete from user where user_id=?
------------------------------------
--@delUserR
-- 删除一个用户
------------------------------------
delete from sys_user_role where user_id=?
------------------------------------
--@delUserG
-- 删除一个用户
------------------------------------
delete from sys_user_group where user_id=?
------------------------------------
--@userRole
-- 查询某个用户拥有的角色
------------------------------------
select r.role_id,r.role_name,case when a.user_id is null then 'N' else 'Y' end  as permit
from sys_role r left join sys_user_role a on r.role_id=a.role_id and a.user_id=?
where r.valid_flag='Y'
------------------------------------
--@userGroup
-- 查询某个用户拥有的分组
------------------------------------
select g.group_id,g.group_name,case when a.user_id is null then 'N' else 'Y' end  as permit
from sys_group g left join sys_user_group a on g.group_id=a.group_id and a.user_id=?
where g.valid_flag='Y'

------------------------------------
--@addUserRole
-- 新增一个用户角色关系
------------------------------------
insert into sys_user_role(user_id,role_id) values(?,?)
------------------------------------
--@delUserRole
-- 删除一个用户角色关系
------------------------------------
delete from sys_user_role where user_id=? and role_id=?
------------------------------------
--@addUserGroup
-- 新增一个用户分组关系
------------------------------------
insert into sys_user_group(user_id,group_id) values(?,?)
------------------------------------
--@delUserGroup
-- 删除一个用户分关系
------------------------------------
delete from sys_user_group where user_id=? and group_id=?
------------------------------------
--@allCodes
--查询所有字典信息
------------------------------------
select DTID,DID,PDID,DNAME from sys_code
------------------------------------
--@addCodeType
--新增一个字典分类
------------------------------------
INSERT INTO sys_code_type (DTID,DTNAME,REMARK) VALUES (?,?,?)
------------------------------------
--@editCodeType
--修改一个字典分类,更新分类名称或者注释
------------------------------------
UPDATE sys_code_type SET DTNAME=?,REMARK=? WHERE DTID=?
------------------------------------
--@delCodeType
--删除一个字典分类
------------------------------------
DELETE FROM sys_code_type WHERE DTID=?
------------------------------------
--@codeTypes
--查询所有字典分类
------------------------------------
SELECT * FROM sys_code_type
------------------------------------
--@addCode
--新增一条指定分类的字典记录
------------------------------------
INSERT INTO sys_code(DID, DNAME, DTID,PDID) VALUES (?,?,?,?)
------------------------------------
--@delCode
--删除指定分类下指定的字典记录
------------------------------------
DELETE FROM sys_code WHERE DID=? and DTID=?
------------------------------------
--@codes
--查询指定分类下的所有字典记录
------------------------------------
SELECT * FROM sys_code WHERE DTID=?

------------------------------------
--@addLog
--新增一条日志信息
------------------------------------
INSERT INTO sys_log(URL,USER_ID,USER_NAME,PARAMIN,DESCP,CREATE_TIME) VALUES (?,?,?,?,?,now())

------------------------------------
--@addMenu
--新增一条菜单信息
------------------------------------
INSERT INTO sys_menu(menu_name, parent_menu_id, menu_order, uri) VALUES (?,?,?,?)
------------------------------------
--@delMenu
--删除一条菜单信息
------------------------------------
DELETE FROM sys_menu WHERE menu_id=?
------------------------------------
--@getUserMenus
--获取用户的菜单项,该sql的逻辑与@permission的逻辑一样。
------------------------------------
select menu_id,menu_name,parent_menu_id from sys_menu where uri in (
	select distinct a.uri from sys_user_group g 
	join sys_user_role r 
	on g.user_id=r.user_id
	join sys_authority a
	on a.rg_id=g.group_id and a.rg='G'
	join sys_authority b
	on b.rg_id=r.role_id and b.rg='R'
	join sys_role s on r.role_id=s.role_id
	join sys_group c on c.group_id=g.group_id
	where s.valid_flag='Y' and c.valid_flag='Y' and r.user_id=?)
order by menu_order
