;(function($) { 
	  /**
	   * 数据收集器
	   * 返回当前jquery对象下 id:value的js对象
	   * 使用方式举例：
	   * html 代码：
	   * <div id="search_panel">
	   *     <label for="orgcode">机构代码：</label>
       *	 <input type="text" id="orgcode" />
       *	 <label for="orgname">会员名称：</label>
       *	 <input type="text" id="orgname" />
	   * </div>
	   * js代码：
	   * var data=$("label+input",$("#search_panel")).collect();
	   * data中的数据就是你要的。要注意的地方是jquery选择出合适的dom对象。
	   */
	  $.fn.collect=function(){
		  var str="{'",data;
		  this.each(function(){
			  str=str+$(this).attr("id")+"':'"+$(this).val()+"','";
		  });
		  str=str.substr(0,str.length-3)+"'}";
		  data=eval("("+str+")");
		  data.toString=function(){
			  var ret="{'",val;
			  for ( var key in this) {
				  if(!$.isFunction(this[key])){
					  val=this[key]?this[key]:"";
					  ret=ret+key+"':'"+val+"','";
				  }
			}
			  return ret.length>2?ret.substr(0,ret.length-3)+"'}":"{}";
		  };
		  return data;
	  };
	  /**
	   * 目的和collect方法一样。数据收集器，只搜集非空数据
	   * 收集指定dom集合中的表单对象的值，如果某个对象的值为null，则不返回该值作为属性。
	   * @return {javaScript object} key:value 形式，带toString方法
	   */
	  $.fn.collectNoNulls=function(){
		  var str="{'",data;
		  this.each(function(){
			  if($(this).val()){
				  str=str+$(this).attr("id")+"':'"+$(this).val()+"','";
			  }
		  });
		  
		  if(str.length>2) str=str.substr(0,str.length-3)+"'}";else str="{}";
		  
		  data=eval("("+str+")");
		  data.toString=function(){
			  var ret="{'",val;
			  for ( var key in this) {
				  if(!$.isFunction(this[key])){
					  val=this[key]?this[key]:"";
					  ret=ret+key+"':'"+val+"','";
				  }
			}
			  return ret.length>2?ret.substr(0,ret.length-3)+"'}":"{}";
		  }
		  return data;
	  };
	  /**
	   * 数据绑定
	   * 将js对象中的key对应到dom的id，并为其赋值,radio,checkbox对应的值必须为boolean类型
	   * 该方法返回修改后的dom对象集合，可以继续使用spread通过其他数据源进行数据绑定。
	   * 例如：$("input[data]",$("#addEvent")).spread(eventRow).spread(row);
	   * 通过2个数据源 eventRow,row对dom集合$("input[data]",$("#addEvent"))进行数据
	   * 绑定。
	   * @param obj 包含key：value键值对的一个对象。
	   */
	  $.fn.spread=function(obj){
		  for ( var key in obj) {
			this.each(function(){
				if(($(this).attr("type")==="text" || $(this).attr("type")==="hidden") && $(this).attr("id")===key){
					$(this).val(obj[key]);
				}
				if(($(this).attr("type")==="radio" || $(this).attr("type")==="checkbox" ) && $(this).attr("id")===key){
					$(this).attr("checked",obj[key]);
				}
			});
		  }
		  return this;
	  };
	  /**
	   * 将table内的数据按列抽出到数组中。数组的元素是原表中一行数据所代表的元素的缩小版。
	   * @param table 被操作的数据表 用包含对象的数组表示 required
	   * @param columns 需要抽取出的列 用包含列字符串的数组表示 required
	   * @return 只包含指定列的数据的缩小版数组。
	   */
	  $.fn.getColumnsFromTable=function(table,columns){
		  var result=[],str="{'",obj;
		  for ( var t = 0,len=table.length; t <len; t++) {
			  for(var i in columns){
				 str=str+ columns[i] +"':'"+table[t][columns[i]]+"','";
			  }
			  if(str.length>2) str=str.substr(0,str.length-3)+"'}";else str="{}";
			  obj=eval("("+str+")");
			  obj.toString=function(){
				  var ret="{'",val;
				  for ( var key in this) {
					  if(!$.isFunction(this[key])){
						  val=this[key]?this[key]:"";
						  ret=ret+key+"':'"+val+"','";
					  }
				}
				  return ret.length>2?ret.substr(0,ret.length-3)+"'}":"{}";
			  };
			  result.push(obj);
			  str="{'";
		  }
		  return result;
	  };
	  /**
	   * 从数据表中取出指定列的数据，并放到一个数组中返回。
	   * @return 指定列数据数组
	   */
	  $.fn.getOneColumnFromTable=function(table,column){
		  var result=[];
		  for ( var t = 0,len=table.length; t <len; t++) {
			  result.push(table[t][column]);
		  }
		  return result;
	  };
	  /**
	   * 从所给js对象中按照指定的键抽取出只包含指定键值的json串。
	   * @return string 包含指定键值的json串。
	   */
	  $.fn.getJSONFromObject=function(obj,keys){
		  var ret="{'",val;
		  for ( var i in keys) {
			  val=obj[keys[i]]?obj[keys[i]]:"";
			  ret=ret+keys[i]+"':'"+val+"','";
		  }
		  return ret.length>2?ret.substr(0,ret.length-3)+"'}":"{}";
	  }
	  /**
	   * 在要校验的标签需要添加 regex ,msg 属性。regex是用来校验输入域中值的
	   * 正则表达式，msg是校验不同过时给出的提示信息。
	   */
	  $.fn.validate=function(){
		  var regex=new RegExp(this.attr("regex"));
		  if(regex.test(this.val())){
			  return true;
		  }else{
			  return this.attr("msg");
		  }
	  }
	  /**
	   * 比较2个js对象是否相等，如果2个js对象有相同的属性且这些相同属性的值也相等，
	   * 则这2个对象就相等。其中以a对象包含的属性为基准，即b对象中的属性数目可以多
	   * 于a对象中的属性数目，但是必须包含a对象中的全部属性。重父级对象继承来的属性
	   * 不会被用于比较。
	   * @param a 比较器中的基准对象
	   * @param b 比较器中的参照对象
	   * @return {javaScript object} the part witch b is difference from a else return null
	   */
	  $.fn.comparator=function(a,b){
		  var str="{'",diff;
		  for(var ai in a){
			  if(!$.isFunction(a[ai])){
				  if(a.hasOwnProperty(ai)&& b.hasOwnProperty(ai) && a[ai]==b[ai]){
					  continue;
				  }else{
					  str=str+ai+"':'"+b[ai]+"','";
				  } 				  
			  }
		  }
		  if(str.length>2){
			  str=str.substr(0,str.length-3)+"'}";
			  diff=eval("("+str+")");
		  } else{
			  diff=null;
		  }
		  return diff;
	  };
})(jQuery);