/**
 * 主要是用来配合消息提示功能。原理为：如果消息中包含占位符，
 * 那么替换消息中占位符的字符串可能还需要进一步的解析。
 * 这样做的原因是，后端校验层可以很容易的获取到无法通过的字段，而要提示为中文则需要为每个验证都写一边提示。
 * 为了简化工作，将字段的中文意思放在了这里。
 * 用来描述数据库中的字段，和字段对应的字段名
 * 以便用来替换返回的消息中的字段占位
 */
(function( $, undefined ) {
	//系统中的提示消息都在这个地方管理，消息中的占位付可以被替换
	 var msg={'group_id':'分组ID',
			  'group_name':'分组名称',
			  'valid_flag':'有效标志',
			  'role_id':'角色ID',
			  'role_name':'角色名称',
			  'f_name':'资源名称',
			  'description':'描述',
			  'user_id':'用户ID',
			  'user_name':'用户名称',
			  'password':'密码'
			 };
	 
	 $.getFieldName=function(key){//add a global function to $ 
		 return !msg[key]?key:msg[key];
	 };
	
}( jQuery) );