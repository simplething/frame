/**frame希望将系统中用到的所有消息都集中到一起进行管理。而且都放在前段。这样做除去集中管理的好处还有节约带宽。
 * 因为有些提示信息只存在于前端。比如针对输入域做的正则检验，只能输入数字等。
 * 为了方便消息的管理，消息编号按如下规则创建:前2位为大写字母，用来对消息做分类，表示某个功能或模块下的消息内容。
 * 第三位表示消息类型(I:提示，S:成功，E：错误/失败,W:警告)。后4位为消息顺序号，递增序列。
 * FS为默认的消息分类标识，表示frame的系统提示,一般都比较常用。后台与消息相关的类为：frame.common.Message.java
 * 以及后端校验消息管理文件：resources/ValidationMessages.properties，
 * 前端字段名称映射文件：frame/js/frame.codeName.js 和 frame/js/jquery.ui.notice.js
 * 
 * 后天返回的消息编号只负责获取到对应的消息内容。有些消息内容是带有占位符的，
 * frame中的消息占位符为了解析方便用数字表示。例如：'FSS0008':'0不能为空!' 其中0表示
 * 将带有编号信息的消息用对应的消息将编号替换掉
 */
(function( $, undefined ) {
	//系统中的提示消息都在这个地方管理，消息中的占位付可以被替换
	 var reg=/^[A-Z0-9]{7}$/,
	     msg={'FSS0000':'操作成功',
			  'FSS0001':'新增成功',
			  'FSS0002':'编辑成功',
			  'FSS0003':'删除成功',
			  'FSS0004':'通过验证',
			  
			  'FSE0000':'操作失败',
			  'FSE0001':'新增失败',
			  'FSE0002':'编辑失败',
			  'FSE0003':'删除失败',
			  'FSE0004':'0不能为空!',
			  'FSE0005':'验证码不对！',
			  'FSE0006':'0已经存在，插入失败！',
			  'FSE0007':'新增失败,失败原因是：0',
			  'FSE0008':'您无权限访问0',
			  
			  'FSI0000':'请选择一条记录！',
			  'FSI0001':'该邮箱已被使用！'
			  
				  
				  
			 };
	 
	 /**
	  * 这是个递归函数,遍历msg字符串中的数字占位符，用给定的替换数组中
	  * 对应的元素替换msg中的占位符。用数字做占位符，数字直接指向替换数组
	  * 中元素的下
	  */
	 function replaceByArray(msg,rp){
		
		  if(!/\d/.test(msg)){
		     return msg;
		  }else{
			  var i=parseInt(msg.match(/\d/),10),
		      rpn=$.getFieldName(rp[i]);
			  msg=msg.replace(/\d/,rpn);
		  }
		  return replaceByArray(msg,rp);
	 }
	 /**
	 * 根据key,获取对应的消息。key 必须是只包含数字的字符串
	 * rps 是替换占位符的字符串，可选。
	 */
	 //key msg对象中的键
	 //rps 可以替换掉msg中的占位符，占位符必须是0开始的自然数
	//判断如果是字符串，则直接认为是消息编码或是消息（如果无法取得编码对应的消息内容）
	//如果是对象，则可以获取到消息编码和消息替换内容列表。消息编码的处理同上。
	 $.fn.getMSG=function(key,rps){
		 
			if(reg.test(key)){
				return replaceByArray(msg[key],rps);
			}else{
				return key;
			}
		};
}( jQuery) );

