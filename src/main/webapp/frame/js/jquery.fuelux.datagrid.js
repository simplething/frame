/**
 *  migrate from fuelux-ui the datagrid
 */
(function( $, undefined ) {
	
	// Relates to thead .sorted styles in datagrid.less
	var SORTED_HEADER_OFFSET = 22;


	// DATAGRID CONSTRUCTOR AND PROTOTYPE

	var Datagrid = function (element, options) {
		this.$element = $(element);
		this.$thead = this.$element.find('thead');
		this.$tfoot = this.$element.find('tfoot');
		this.$footer = this.$element.find('tfoot th');
		this.$footerchildren = this.$footer.children().show().css('visibility', 'hidden');
		this.$topheader = this.$element.find('thead th');
		this.$searchcontrol = this.$element.find('.datagrid-search');
		this.$filtercontrol = this.$element.find('.filter');
		this.$pagesize = this.$element.find('.grid-pagesize');
		this.$pageinput = this.$element.find('.grid-pager input');
		this.$pagedropdown = this.$element.find('.grid-pager .dropdown-menu');
		this.$prevpagebtn = this.$element.find('.grid-prevpage');
		this.$nextpagebtn = this.$element.find('.grid-nextpage');
		this.$pageslabel = this.$element.find('.grid-pages');
		this.$countlabel = this.$element.find('.grid-count');
		this.$startlabel = this.$element.find('.grid-start');
		this.$endlabel = this.$element.find('.grid-end');

		this.$tbody = $('<tbody>').insertAfter(this.$thead);
		this.$colheader = $('<tr>').appendTo(this.$thead);

		this.options = $.extend(true, {}, $.fn.datagrid.defaults, options);//在这里将默认参数跟用户指定参数混合起来

		// Shim until v3 -- account for FuelUX select or native select for page size:
		if (this.$pagesize.hasClass('select')) {
			this.options.dataOptions.pageSize = parseInt(this.$pagesize.select('selectedItem').value, 10);
		} else {
			this.options.dataOptions.pageSize = parseInt(this.$pagesize.val(), 10);
		}

		// Shim until v3 -- account for older search class:
		if (this.$searchcontrol.length <= 0) {
			this.$searchcontrol = this.$element.find('.search');
		}

		this.columns = this.options.dataSource.columns();

		//事件绑定
		this.$nextpagebtn.on('click', $.proxy(this.next, this));
		this.$prevpagebtn.on('click', $.proxy(this.previous, this));
		this.$searchcontrol.on('searched cleared', $.proxy(this.searchChanged, this));
		this.$filtercontrol.on('changed', $.proxy(this.filterChanged, this));
		this.$colheader.on('click', 'th', $.proxy(this.headerClicked, this));
		this.$tbody.on('itemSelected','tr',this.options.itemSelected);//为gird中每一行绑定itemSelected事件，在被选中的时候触发
		this.$tbody.on('itemDeselected','tr',this.options.itemDeselected);//为gird中每一行绑定itemDeselected事件，在被取消选中的时候触发
		this.$tbody.on('itemDblclicked','tr',this.options.itemDblclicked);//为gird中每一行绑定itemDblclicked事件，在被双击时触发
		
		//this._clearTimeout=null; //定义一个时延器
		
		//为以后生成的行绑定单击事件
		this.$tbody.on('click','tr',function(e){
			// 取消上次延时未执行的方法
		//	clearTimeout(this._clearTimeout);
		//	this._clearTimeout=setTimeout(function(){
				var checked=$(this).data("checked"),$chk=$('input',this),target=e.target,ds=options.dataSource._seleted;
				if(checked){
					if($(target).is("td") || $(target).is("i")){
						$(this).removeData("checked");
						$(this).removeAttr("style");
						ds.splice(ds.indexOf($(this).attr("row_id")),1);//清除取消选择的行号
					}
					try{
						$(this).trigger("itemDeselected");
					}catch(err){/*donothing*/}
				}else if(!$(target).is("input") ){
					
					$(this).data("checked",true);
					$(this).css("background-color","#F7ED3B");
					ds.push($(this).attr("row_id"));//保存被选择的行号
//					console.log("被选择了");
					try{
						//console.log(this.options.dataSource.getSelectedRow());
						$(this).trigger("itemSelected");
					}catch(err){/*donothing*/}
				}
				if($(target).is("td")){
					$chk.attr("checked",!checked);
					$chk.checkbox().change();//a checkbox method always worked as radio type
				}
		//	},300);
		});
		
		this.$tbody.on('dblclick','tr',function(e){
			// 取消上次延时未执行的方法
		//	clearTimeout(this._clearTimeout);
			var ds=options.dataSource._seleted;
			ds.push($(this).attr("row_id"));//保存被选择的行号
			try{
				//console.log(this.options.dataSource.getSelectedRow());
				$(this).trigger("itemDblclicked");
			}catch(err){/*donothing*/}
		});

		if(this.$pagesize.hasClass('select')) {
			this.$pagesize.on('changed', $.proxy(this.pagesizeChanged, this));
		} else {
			this.$pagesize.on('change', $.proxy(this.pagesizeChanged, this));
		}

		this.$pageinput.on('change', $.proxy(this.pageChanged, this));

		this.renderColumns();

		if (this.options.stretchHeight) this.initStretchHeight();

		this.renderData();
	};

	Datagrid.prototype = {

		constructor: Datagrid,

		renderColumns: function () {
			var self = this,colHTML='',colen=this.columns.length;
			
			if(this.options.rowNum ){//行号
				colHTML+='<th data-property="" style="width:10px;"></th>';
				colen++;
			}
			
			if(this.options.selectedType==='radio'){//单选框
				colHTML+='<th data-property="" style="width:10px;"></th>';
				colen++;
			}else if(this.options.selectedType==='checkbox'){//复选框
				colHTML+='<th data-property="" style="width:10px;"><label class="checkbox checkbox-custom"><input type="checkbox"  id="fuelux_grid_checkbox_selectedAll"><i class="checkbox"></i></label></th>';
				colen++;
				$("table").on('click',"#fuelux_grid_checkbox_selectedAll",function(){
					var ck=$(this)[0]['checked'];
					$("tbody input[name='fuelux_grid_checkbox']").each(function(){
						if(ck!==$(this)[0]['checked']){
							$(this).checkbox('toggle');
						}
					});
				});
			}
			
			this.$footer.attr('colspan', colen);
			this.$topheader.attr('colspan', colen);
			

			$.each(this.columns, function (index, column) {
				colHTML += '<th data-property="' + column.property + '"';
				if (column.sortable) colHTML += ' class="sortable"';
				colHTML += '>' + column.label + '</th>';
			});

			self.$colheader.append(colHTML);
		},

		updateColumns: function ($target, direction) {
			this._updateColumns(this.$colheader, $target, direction);

			if (this.$sizingHeader) {
				this._updateColumns(this.$sizingHeader, this.$sizingHeader.find('th').eq($target.index()), direction);
			}
		},

		_updateColumns: function ($header, $target, direction) {
			var className = (direction === 'asc') ? 'icon-chevron-up' : 'icon-chevron-down';
			$header.find('i.datagrid-sort').remove();
			$header.find('th').removeClass('sorted');
			$('<i>').addClass(className + ' datagrid-sort').appendTo($target);
			$target.addClass('sorted');
		},

		updatePageDropdown: function (data) {
			var pageHTML = '';

			for (var i = 1; i <= data.pages; i++) {
				pageHTML += '<li><a>' + i + '</a></li>';
			}

			this.$pagedropdown.html(pageHTML);
		},

		updatePageButtons: function (data) {
			if (data.page === 1) {
				this.$prevpagebtn.attr('disabled', 'disabled');
			} else {
				this.$prevpagebtn.removeAttr('disabled');
			}

			if (data.page === data.pages) {
				this.$nextpagebtn.attr('disabled', 'disabled');
			} else {
				this.$nextpagebtn.removeAttr('disabled');
			}
		},

		renderData: function () {
			var self = this,chkAll=$("#fuelux_grid_checkbox_selectedAll");
			
				if(chkAll[0] && true==chkAll[0]['checked']){
					chkAll.checkbox('toggle');
				}

			this.$tbody.html(this.placeholderRowHTML(this.options.loadingHTML));

			this.options.dataSource.data(this.options.dataOptions, function (data) {
				var itemdesc = (data.count === 1) ? self.options.itemText : self.options.itemsText;
				var rowHTML = '';

				self.$footerchildren.css('visibility', function () {
					return (data.count > 0) ? 'visible' : 'hidden';
				});

				self.$pageinput.val(data.page);
				self.$pageslabel.text(data.pages);
				self.$countlabel.text(data.count + ' ' + itemdesc);
				self.$startlabel.text(data.start);
				self.$endlabel.text(data.end);

				self.updatePageDropdown(data);
				self.updatePageButtons(data);

				$.each(data.data, function (index, row) {
					rowHTML += '<tr class="datagrid-tr" row_id='+index+'>';
					if(self.options.rowNum){//生成可见的行号
						rowHTML+='<td>'+index+'</td>';
					}
					if(self.options.selectedType==='radio'){
						rowHTML+='<td><label class="radio radio-custom"><input type="radio" name="fuelux_grid_select_radio"><i class="radio"></i></label></td>';
					}else if(self.options.selectedType==='checkbox'){
						rowHTML+='<td><label class="checkbox checkbox-custom"><input type="checkbox" name="fuelux_grid_checkbox"><i class="checkbox"></i></label></td>';
					}
					$.each(self.columns, function (index, column) {
						if($.isFunction(column.render)){
						   rowHTML += '<td>' + column.render(row) + '</td>';
						}else{
						   rowHTML += '<td>' + row[column.property] + '</td>';
						}
					});
					
					rowHTML += '</tr>';
				});

				if (!rowHTML) rowHTML = self.placeholderRowHTML('0 ' + self.options.itemsText);

				self.$tbody.html(rowHTML);
				self.stretchHeight();
				self.$element.trigger('loaded');
				
				//嵌入到datagrid中的在这里初始化radio or checkbox
				if(self.options.selectedType==='radio'){
					$('.radio-custom > input[type=radio]').each(function () {
						var $this = $(this);
						if ($this.data('radio')) return;
						$this.radio($this.data());
					});
				}else if(self.options.selectedType==='checkbox'){
					$('.checkbox-custom > input[type=checkbox]').each(function () {
						var $this = $(this);
						if ($this.data('checkbox')) return;
						$this.checkbox($this.data());
					});
				}
				
			});
			
		},

		placeholderRowHTML: function (content) {
			return '<tr><td style="text-align:center;padding:20px;border-bottom:none;" colspan="' +
				this.columns.length + '">' + content + '</td></tr>';
		},

		headerClicked: function (e) {
			var $target = $(e.target);
			if (!$target.hasClass('sortable')) return;

			var direction = this.options.dataOptions.sortDirection;
			var sort = this.options.dataOptions.sortProperty;
			var property = $target.data('property');

			if (sort === property) {
				this.options.dataOptions.sortDirection = (direction === 'asc') ? 'desc' : 'asc';
			} else {
				this.options.dataOptions.sortDirection = 'asc';
				this.options.dataOptions.sortProperty = property;
			}

			this.options.dataOptions.pageIndex = 0;
			this.updateColumns($target, this.options.dataOptions.sortDirection);
			this.renderData();
		},

		pagesizeChanged: function (e, pageSize) {
			if(pageSize) {
				this.options.dataOptions.pageSize = parseInt(pageSize.value, 10);
			} else {
				this.options.dataOptions.pageSize = parseInt($(e.target).val(), 10);
			}

			this.options.dataOptions.pageIndex = 0;
			this.renderData();
		},

		pageChanged: function (e) {
			var pageRequested = parseInt($(e.target).val(), 10);
			pageRequested = (isNaN(pageRequested)) ? 1 : pageRequested;
			var maxPages = this.$pageslabel.text();
		
			this.options.dataOptions.pageIndex = 
				(pageRequested > maxPages) ? maxPages - 1 : pageRequested - 1;

			this.renderData();
		},

		searchChanged: function (e, search) {
			this.options.dataOptions.search = search;
			this.options.dataOptions.pageIndex = 0;
			this.renderData();
		},

		filterChanged: function (e, filter) {
			this.options.dataOptions.filter = filter;
			this.options.dataOptions.pageIndex = 0;
			this.renderData();
		},

		previous: function () {
			this.options.dataOptions.pageIndex--;
			this.renderData();
		},

		next: function () {
			this.options.dataOptions.pageIndex++;
			this.renderData();
		},

		reload: function () {
			this.options.dataOptions.pageIndex = 0;
			this.options.dataOptions.reload=true;//add reload mark
			this.renderData();
		},

		initStretchHeight: function () {
			this.$gridContainer = this.$element.parent();

			this.$element.wrap('<div class="datagrid-stretch-wrapper">');
			this.$stretchWrapper = this.$element.parent();

			this.$headerTable = $('<table>').attr('class', this.$element.attr('class'));
			this.$footerTable = this.$headerTable.clone();

			this.$headerTable.prependTo(this.$gridContainer).addClass('datagrid-stretch-header');
			this.$thead.detach().appendTo(this.$headerTable);

			this.$sizingHeader = this.$thead.clone();
			this.$sizingHeader.find('tr:first').remove();

			this.$footerTable.appendTo(this.$gridContainer).addClass('datagrid-stretch-footer');
			this.$tfoot.detach().appendTo(this.$footerTable);
		},

		stretchHeight: function () {
			if (!this.$gridContainer) return;

			this.setColumnWidths();

			var targetHeight = this.$gridContainer.height();
			var headerHeight = this.$headerTable.outerHeight();
			var footerHeight = this.$footerTable.outerHeight();
			var overhead = headerHeight + footerHeight;

			this.$stretchWrapper.height(targetHeight - overhead);
		},

		setColumnWidths: function () {
			if (!this.$sizingHeader) return;

			this.$element.prepend(this.$sizingHeader);

			var $sizingCells = this.$sizingHeader.find('th');
			var columnCount = $sizingCells.length;

			function matchSizingCellWidth(i, el) {
				if (i === columnCount - 1) return;

				var $el = $(el);
				var $sourceCell = $sizingCells.eq(i);
				var width = $sourceCell.width();

				// TD needs extra width to match sorted column header
				if ($sourceCell.hasClass('sorted') && $el.prop('tagName') === 'TD') width = width + SORTED_HEADER_OFFSET;

				$el.width(width);
			}

			this.$colheader.find('th').each(matchSizingCellWidth);
			this.$tbody.find('tr:first > td').each(matchSizingCellWidth);

			this.$sizingHeader.detach();
		}
	};


	// DATAGRID PLUGIN DEFINITION

	$.fn.datagrid = function (option) {
//		return this.each(function () {
			var $this = $(this),
			    ds=option.dataSource,
			    data,
			    options = typeof option === 'object' && option;
			//如果是从server获取数据，则用资源路径作为grid的data key。
			if(typeof option === 'object' && ds._server  ){
				$this.find('tbody').remove();//移除因为前一次构建grid产生的dom元素
				$this.find('thead>tr>th[data-property]').remove();
				return new Datagrid(this, options);
//				data = $this.data(ds._url)
//				if (!data) {
//					$this.find('tbody').remove();//移除因为前一次构建grid产生的dom元素
//					$this.find('thead>tr>th[data-property]').remove();
//					$this.data(ds._url, (data = new Datagrid(this, options)));
//				}
			}else if(typeof option === 'object'){
				data = $this.data('datagrid');
				if (!data) $this.data('datagrid', (data = new Datagrid(this, options)));
				return data;
			}
			
			if (typeof option === 'string') data[option]();
//		});
	};

	$.fn.datagrid.defaults = {
		dataOptions: { pageIndex: 0, pageSize: 10 },
		loadingHTML: '<div class="progress progress-striped active" style="width:50%;margin:auto;"><div class="bar" style="width:100%;"></div></div>',
		itemsText: 'items',
		itemText: 'item',
		rowNum:false,
		selectOnRow:true,//单击一行时是否选中 增加鼠标移动到行上时行的颜色的变化
		singleSelect:true,//false is multipe selected
		selectedType:null//checkbox | radio  null means do nothing about the face
	};

	$.fn.datagrid.Constructor = Datagrid;
	
}( jQuery ) );