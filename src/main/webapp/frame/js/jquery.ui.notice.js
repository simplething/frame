/**
 * 提供一个消息展现概念的实现，简单来说就是一个弹出框，这里是一个从页面顶部滑出的消息条。
 * 展示完后会自动缩回去。目标是减少用户点击[确定]的操作。
 */
(function( $, undefined ) {
	
	var Notice=function(element, message,options){
		var $this=$(element);
		
		this.options = $.extend({}, $.fn.notice.defaults, options);
		
		
		if(!this.notice){
			$this.prepend('<div class="hidden" id="frame-notice"></div>');
			this.notice=$this.find("#frame-notice");
		}
		
		if(message){//为了支持创建一个空对象
			buildMsg(this,message);
		}
		
	};
	//message is an object
	function buildMsg(noticer,message){
		var mtype=message.msg.charAt(2),
		    info=$.fn.getMSG(message.msg,message.replacer);
		
		switch(mtype){//消息类型控制
			case 'S':
				mtype='sussess';
			    break;
			case 'E':
				mtype='danger';
			    break;
			case 'I':
				mtype='info';
			    break;
			case 'W':
				mtype='warn';
			    break;
			default:
				mtype='info';
		}
		
		noticer.notice.prepend('<div class="alert alert-'+mtype+'"><strong class="text-center">'+info+'</strong></div>');
		
	};
	
	Notice.prototype={
		constructor	:Notice,
		//接受字符串形式的消息码
		setMessage:function(message,rp){
			var msg={};
			msg.msg=message;
			msg.replacer=rp;
			buildMsg(this,msg);
			return this;
		},
		show:function(speed){
			if(speed!=null){
				this.notice.slideDown(speed);
			}else{
				this.notice.slideDown(this.options.fadeTime);
			}
			return this;
		},
		hide:function(speed){
			var $this=this;
			if(speed!=null){
				this.notice.slideUp(speed,empty);
			}else{
				this.notice.slideUp(this.options.fadeTime,empty);
			}
			
			function empty(){
				$($this.notice).empty();
			}
			
			return this;
		}
	};
	
	
	
	/**
	 * 绑定到jquery。message is an object
	 * 如果message的类型是对象，那么option如果有值的话应该是对象，第3个参数无效。
	 */
	$.fn.notice=function(message,options,op2){
		
		var n=null;
		
		if($.type(message)==="object" && ($.type(options)==="object"||$.type(options)==="undefined") ){
				n= new Notice(this,message,options);
		}
		
		if(!message){//创建一个空的notice
			n= new Notice(this);
		}
		
		
		if(!n){//如果n没有被创建，则返回异常！
			n= new Notice(this,{msg:"notice创建失败！请该对象的是用说明！"});
		}
		
//		if($.type(message)==="string" && ($.type(options)==="array"||$.type(options)==="undefined")){
//		var msg={};
//		msg.msg=message;
//		msg.replacer=options;
//		n= new Notice(this,msg,op2);
//	}
		
		return n;
		
	};
	
	$.fn.notice.defaults={
		autoHide:true,//是否自动隐藏
		fadeTime:'slow'//类似与jquery fadeIn或out
		//type:'info'//notice type info,warning,danger 类型的定义先不要，当前没有必要，这样做可以强制要求消息码的使用
	};
	
	$.fn.notice.Constructor=Notice;
	
}( jQuery ) );