<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
   <table id="codeGrid" class="table table-bordered datagrid">
			<thead>
			<tr>
				<th>
					<div class="datagrid-header-left">
						 <button class="btn btn-success" id="addCode">新增</button>
						 <button class="btn btn-danger" id="delCode">删除</button>
						 <small class="text-right text-info">当前类型：<span id="currentType">?</span></small>
					</div>
				</th>
			</tr>
			</thead>
	
			<tfoot>
			<tr>
				<th>
					<div class="datagrid-footer-left" style="display:none;">
						<div class="grid-controls">
							<span>
								<span class="grid-start"></span> -
								<span class="grid-end"></span> of
								<span class="grid-count"></span>
							</span>
							<div class="select grid-pagesize" data-resize="auto">
								<button data-toggle="dropdown" class="btn dropdown-toggle">
									<span class="dropdown-label"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li data-value="5" data-selected="true"><a href="#">5</a></li>
									<li data-value="10"><a href="#">10</a></li>
									<li data-value="20"><a href="#">20</a></li>
									<li data-value="50"><a href="#">50</a></li>
									<li data-value="100"><a href="#">100</a></li>
								</ul>
							</div>
							<span>每页</span>
						</div>
					</div>
					<div class="datagrid-footer-right" style="display:none;">
						<div class="grid-pager">
							<button type="button" class="btn grid-prevpage"><i class="icon-chevron-left"></i></button>
							<span>第</span>
	
							<div class="input-append dropdown combobox">
								<input class="span1" style="width: 49px;" type="text">
								<button class="btn" data-toggle="dropdown"><i class="caret"></i></button>
								<ul class="dropdown-menu"></ul>
							</div>
							<span>页 &nbsp;&nbsp;共 <span class="grid-pages"></span>页</span>
							<button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
						</div>
					</div>
				</th>
			</tr>
			</tfoot>
	
		</table>
       <div id="code_dialog" style="display: none;" title="新增一个字典">
         <form class="form-horizontal" id="codeInfo">
             <input type="hidden" id="codedtid" name="dtid">
		     <div class="control-group">
		        <label class="control-label" for="did">字典ID</label>
			    <div class="controls">
			      <input type="text" id="did" name="did" placeholder="字典ID">
			    </div>
		     </div>
		     <div class="control-group">
		        <label class="control-label" for="dname">字典名称</label>
			    <div class="controls">
			      <input type="text" id="dname" name="dname" placeholder="字典名称">
			    </div>
		     </div>
		     <div class="control-group">
		        <label class="control-label" for="pdid">父ID</label>
			    <div class="controls">
			      <input type="text" id="pdid" name="pdid" placeholder="父ID，方便构建树结构">
			    </div>
		     </div>
		</form>
    </div>
   <script>
   
      jQuery(function($){
        var currentCodeType=$("body").data("currentCodeType")||{dtid: null, dtname: "?", remark: null},
            dsCode=new AjaxDataSource({
		            url: "mgr/getCodes.do",  // values from Django view 
			        columns: [
						{property: 'did',label: '字典ID',sortable: true},
						{property: 'dname',label: '字典名称',sortable: true},
						{property: 'pdid',label: '父ID',sortable: true}
					],
					queryData:{'dtid':currentCodeType.dtid},
					server:true,
			        stretchHeight: true
			    }),codeurl="",
			    codegrid=$('#codeGrid').datagrid({
						dataSource: dsCode,
						itemsText:" 条 "
					});
		 
		 $("#currentType").html(currentCodeType.dtname);
				// Dialog
				
				
			    $("#code_dialog").dialog({
			        autoOpen: false,
			        width: 600,
			        buttons: {
			            "Ok": function () {
			                $.post(codeurl,$("#codeInfo").serialize(),function(){
			                	codegrid.reload();
							},'json');
			                $(this).dialog("close");
			                
			            },
			            "Cancel": function () {
			                $(this).dialog("close");
			            }
			        }
			    });
			    
				$("#addCode").click(function(){
				   $("#codedtid").val(currentCodeType.dtid);
				   $("#codeInfo").resetForm();
				   codeurl="mgr/addCode.do";
				   $("#code_dialog").dialog('open');
	                return false;
				});
				
				$("#delCode").click(function(){
				  var data=dsCode.getSelectedRow();
				  if(data){
				    $.post("mgr/delCode.do",{"did":data.did,"dtid":currentCodeType.dtid},function(){
				    	codegrid.reload();
					},'json');
				  }else{
					  f$.notice("FSI0000");
				  }
				});
			
      });
			
   </script>