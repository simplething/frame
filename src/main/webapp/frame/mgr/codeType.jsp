<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

		<table id="codeTypeGrid" class="table table-bordered datagrid">
			<thead>
			<tr>
				<th>
					<div class="datagrid-header-left">
						 <button class="btn btn-success" id="addCodeType">新增</button>
						 <button class="btn btn-warning" id="editCodeType">修改</button>
						 <button class="btn btn-danger" id="delCodeType">删除</button>
						 <button class="btn btn-info" id="refreshCache">更新到缓存</button>
					</div>
				</th>
			</tr>
			</thead>
	
			<tfoot>
			<tr>
				<th>
					<div class="datagrid-footer-left" style="display:none;">
						<div class="grid-controls">
							<span>
								<span class="grid-start"></span> -
								<span class="grid-end"></span> of
								<span class="grid-count"></span>
							</span>
							<div class="select grid-pagesize" data-resize="auto">
								<button data-toggle="dropdown" class="btn dropdown-toggle">
									<span class="dropdown-label"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li data-value="5" data-selected="true"><a href="#">5</a></li>
									<li data-value="10"><a href="#">10</a></li>
									<li data-value="20"><a href="#">20</a></li>
									<li data-value="50"><a href="#">50</a></li>
									<li data-value="100"><a href="#">100</a></li>
								</ul>
							</div>
							<span>每页</span>
						</div>
					</div>
					<div class="datagrid-footer-right" style="display:none;">
						<div class="grid-pager">
							<button type="button" class="btn grid-prevpage"><i class="icon-chevron-left"></i></button>
							<span>第</span>
	
							<div class="input-append dropdown combobox">
								<input class="span1" style="width: 49px;" type="text">
								<button class="btn" data-toggle="dropdown"><i class="caret"></i></button>
								<ul class="dropdown-menu"></ul>
							</div>
							<span>页 &nbsp;&nbsp;共 <span class="grid-pages"></span>页</span>
							<button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
						</div>
					</div>
				</th>
			</tr>
			</tfoot>
	
		</table>
       <div id="codeType_dialog" style="display: none;" title="新增一个字典类型">
        <form class="form-horizontal" id="codeTypeInfo">
		     <div class="control-group">
		        <label class="control-label" for="dtid">字典类型ID</label>
			    <div class="controls">
			      <input type="text" id="dtid" name="dtid" placeholder="字典类型ID">
			    </div>
		     </div>
		     <div class="control-group">
		        <label class="control-label" for="dtname">字典类型名称</label>
			    <div class="controls">
			      <input type="text" id="dtname" name="dtname" placeholder="字典类型名称">
			    </div>
		     </div>
		     <div class="control-group">
		        <label class="control-label" for="remark">备注</label>
			    <div class="controls">
			      <textarea rows="4" id="remark" name="remark"></textarea>
			    </div>
		     </div>
		</form>
    </div>
  
   <script>
   
      jQuery(function($){
         var dsCodeType=new AjaxDataSource({
	            url: "mgr/getCodeTypes.do",  // values from Django view 
		        columns: [
					{property: 'dtid',label: '字典类型ID',sortable: true},
					{property: 'dtname',label: '字典类型名称',sortable: true},
					{property: 'remark',label: '备注',sortable: true}
				],
				server:true,
		        stretchHeight: true
		    }),codeTypeurl="",
		    codeTypegrid=$('#codeTypeGrid').datagrid({
					dataSource: dsCodeType,
					itemsText:" 条 ",
					singleSelect:false,
					itemDblclicked:function(){
				       //  var rdata=dsCodeType.getSelectedRow();
				       $("body").data("currentCodeType",dsCodeType.getSelectedRow());
					   $('#myTab a:last').tab('show');
				    }
				});
			
			// Dialog
			
		    $("#codeType_dialog").dialog({
		        autoOpen: false,
		        width: 600,
		        buttons: {
		            "Ok": function () {
		                $.post(codeTypeurl,$("#codeTypeInfo").serialize(),function(){
		                	codeTypegrid.reload();
						},'json');
		                $(this).dialog("close");
		                
		            },
		            "Cancel": function () {
		                $(this).dialog("close");
		            }
		        }
		    });
		    
			$("#addCodeType").click(function(){
			   $("#codeTypeInfo").resetForm();
			   codeTypeurl="mgr/addCodeType.do";
			   $("#codeType_dialog").dialog('open');
                return false;
			});
			
			$("#editCodeType").click(function(){
			  var data=dsCodeType.getSelectedRow();
			  if(data){
			      $("#dtid").val(data.dtid);
				  $("#dtname").val(data.dtname);
				  $("#remark").val(data.remark);
				  
				  codeTypeurl="mgr/editCodeType.do";
				  $("#codeType_dialog").dialog('open');
			  }else{
				  f$.notice("FSI0000");
			  }
                return false;
			});
			$("#delCodeType").click(function(){
			  var data=dsCodeType.getSelectedRow();
			  if(data){
			    $.post("mgr/delCodeType.do",{"dtid":data.dtid},function(){
			    	codeTypegrid.reload();
				},'json');
			  }else{
				  f$.notice("FSI0000");
			  }
			});
			
			//将数据库中的数据更新到前端 
			$("#refreshCache").click(function(){
				$("body").data("codes",null);//将前端缓冲的数据清空
				f$.getCodeInfo();//重新获取缓冲数据
			});
      });
   </script>