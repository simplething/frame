<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<div class="page-header">
            <h3>字典管理</h3>
 </div>
<ul class="nav nav-tabs" id="myTab">
  <li><a class="active" href="#typeTab" data-toggle="tab">字典类型</a></li>
  <li><a href="#codeTab" data-toggle="tab">字典信息</a></li>
</ul>

<div class="tab-content">
  <div class="tab-pane active" id="typeTab"></div>
  <div class="tab-pane" id="codeTab">字典信息</div>
</div>

<script type="text/javascript">
  $(function () {
    $('#myTab a:first').tab('show');
    //默认加载第一个标签的页面
     $("#typeTab").load("frame/mgr/codeType.jsp");
   
   // $('#myTab a:last').tab('show');
    //默认加载第一个标签的页面
   //  $("#codeTab").load("frame/mgr/code.jsp");
     
    $('a[href="#typeTab"]').on('show', function (e) {
  	   $("#typeTab").load("frame/mgr/codeType.jsp");
    });
    $('a[href="#codeTab"]').on('show', function (e) {
	    $("#codeTab").load("frame/mgr/code.jsp");
    });
  })
</script>