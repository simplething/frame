<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
	  <div class="page-header">
            <h3>用户分组</h3>
      </div>
		<table id="MyGrid" class="table table-bordered datagrid">
			<thead>
			<tr>
				<th>
					<div class="datagrid-header-left">
						 <button class="btn btn-success" id="add">新增</button>
						 <button class="btn btn-warning" id="edit">修改</button>
						 <button class="btn btn-danger" id="del">删除</button>
					</div>
				</th>
			</tr>
			</thead>
	
			<tfoot>
			<tr>
				<th>
					<div class="datagrid-footer-left" style="display:none;">
						<div class="grid-controls">
							<span>
								<span class="grid-start"></span> -
								<span class="grid-end"></span> of
								<span class="grid-count"></span>
							</span>
							<div class="select grid-pagesize" data-resize="auto">
								<button data-toggle="dropdown" class="btn dropdown-toggle">
									<span class="dropdown-label"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li data-value="5" data-selected="true"><a href="#">5</a></li>
									<li data-value="10"><a href="#">10</a></li>
									<li data-value="20"><a href="#">20</a></li>
									<li data-value="50"><a href="#">50</a></li>
									<li data-value="100"><a href="#">100</a></li>
								</ul>
							</div>
							<span>每页</span>
						</div>
					</div>
					<div class="datagrid-footer-right" style="display:none;">
						<div class="grid-pager">
							<button type="button" class="btn grid-prevpage"><i class="icon-chevron-left"></i></button>
							<span>第</span>
	
							<div class="input-append dropdown combobox">
								<input class="span1" style="width: 49px;" type="text">
								<button class="btn" data-toggle="dropdown"><i class="caret"></i></button>
								<ul class="dropdown-menu"></ul>
							</div>
							<span>页 &nbsp;&nbsp;共 <span class="grid-pages"></span>页</span>
							<button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
						</div>
					</div>
				</th>
			</tr>
			</tfoot>
	
		</table>
<div id="group_dialog" style="display: none;" title="新增一个分组">
        <form class="form-horizontal" id="info">
             <input type="hidden" id="group_id" name="group_id" value=0>
		     <div class="control-group">
		        <label class="control-label" for="group_name">分组名</label>
			    <div class="controls">
			      <input type="text" id="group_name" name="group_name" placeholder="分组名称">
			    </div>
		     </div>
		     <div class="control-group">
		        <div class="controls">
		          <label class="checkbox checkbox-custom">
		            <input type="checkbox" id="valid_flag" name="valid_flag" checked="checked"/>
		            <i class="checkbox"></i>是否有效
		          </label>
		        </div>
		    </div>
		</form>
    </div>
   <script>
     jQuery(function($){
      var dsGroups=new AjaxDataSource({
		        url: "mgr/groups.do",  // values from Django view
		        columns: [
					{property: 'group_name',label: '分组名',sortable: true},
					{property: 'valid_flag',label: '是否有效',sortable: true,render:function(r){
					   return f$.getCodeInfo("yn",r.valid_flag);
					}}
				],
				server:true,
		        stretchHeight: true
		    }),url="";
			
			
		  
		  var grid=$('#MyGrid').datagrid({
				dataSource: dsGroups,
				itemsText:" 条 "
			});
			
			// Dialog
		    $("#group_dialog").dialog({
		        autoOpen: false,
		        width: 600,
		        buttons: {
		            "Ok":  function () {
		                $.post(url,$("#info").serialize(),function(){
		                    grid.reload();
						},'json');
		                $(this).dialog("close");
		                
		            },
		            "Cancel": function () {
		                $(this).dialog("close");
		            }
		        }
		    });
		    
			$("#add").click(function(){
			   $("#info").resetForm();
			    url="mgr/addGroups.do";
			   $("#group_dialog").dialog('open');
              return false;
			});
			$("#edit").click(function(){
			  var data=dsGroups.getSelectedRow();
			  if(data){
			      $("#group_id").val(data.group_id);
				  $("#group_name").val(data.group_name);
				  
				  if(($("#valid_flag:checked").length==1 && data.valid_flag=='N')||
				     ($("#valid_flag:checked").length==0 && data.valid_flag=='Y') ){
				    $("#valid_flag").checkbox('toggle');
				  }
				  url="mgr/editGroups.do";
				  $("#group_dialog").dialog('open');
			  }else{
			      f$.notice("FSI0000");
			  }
                return false;
			});
			$("#del").click(function(){
			  var data=dsGroups.getSelectedRow();
			  if(data){
			    $.post("mgr/delGroups.do",{"group_id":data.group_id},function(){
				     grid.reload();
				},'json');
			  }else{
				  f$.notice("FSI0000");
			  }
			});
			
			$(".nav-list >li >a").parent().click(function(){
			   $(this).siblings().not(".divider").not(".nav-header").removeAttr("class");
			   $(this).attr("class","active");
			});
			
			$("#valid_flag").checkbox();
     });
			
   
   </script>
