<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
     <div class="page-header">
            <h3>日志管理</h3>
      </div>
		<table id="logGrid" class="table table-bordered datagrid">
			<thead>
			<tr>
				<th>
					<div class="datagrid-header-left">
						  <form class="form-inline " id="logquery">
							      <input type="text" id="url" name="url" placeholder="请求路径">
							      <input type="text" id="user_name" name="user_name" placeholder="用户名">
							      <input id="start" name="start" size="16" type="text"  placeholder="开始时间">
								  <input id="end" name="end"  size="16" type="text"  placeholder="结束时间">
							      <button id="submit" type="button" class="btn">查询</button>
							      <button type="reset" class="btn">重置</button>
						 </form>
					</div>
				</th>
			</tr>
			</thead>
	
			<tfoot>
			<tr>
				<th>
					<div class="datagrid-footer-left" style="display:none;">
						<div class="grid-controls">
							<span>
								<span class="grid-start"></span> -
								<span class="grid-end"></span> of
								<span class="grid-count"></span>
							</span>
							<div class="select grid-pagesize" data-resize="auto">
								<button data-toggle="dropdown" class="btn dropdown-toggle">
									<span class="dropdown-label"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li data-value="5" data-selected="true"><a href="#">5</a></li>
									<li data-value="10"><a href="#">10</a></li>
									<li data-value="20"><a href="#">20</a></li>
									<li data-value="50"><a href="#">50</a></li>
									<li data-value="100"><a href="#">100</a></li>
								</ul>
							</div>
							<span>每页</span>
						</div>
					</div>
					<div class="datagrid-footer-right" style="display:none;">
						<div class="grid-pager">
							<button type="button" class="btn grid-prevpage"><i class="icon-chevron-left"></i></button>
							<span>第</span>
	
							<div class="input-append dropdown combobox">
								<input class="span1" style="width: 49px;" type="text">
								<button class="btn" data-toggle="dropdown"><i class="caret"></i></button>
								<ul class="dropdown-menu"></ul>
							</div>
							<span>页 &nbsp;&nbsp;共 <span class="grid-pages"></span>页</span>
							<button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
						</div>
					</div>
				</th>
			</tr>
			</tfoot>
	
		</table>
    
   <script>
       
      jQuery(function($){
         var   url=$('#url').val(),
               user_name=$('#user_name').val(),
               start=$('#start').val()?new Date($('#start').val()).getTime():'',
               end=$('#end').val()?new Date($('#end').val()).getTime():'',
               dsLogs=new AjaxDataSource({
	            url: "mgr/getLogs.do",  // values from Django view 
		        columns: [
                    {property: 'user_name',label: '用户',sortable: true},
					{property: 'url',label: '请求路径',sortable: true},
					{property: 'paramin',label: '请求参数',sortable: false},
					{property: 'create_time',label: '创建时间',sortable: true,render:function(r){
						var date=new Date(r.create_time);
					   return date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate()
					          +" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
					}},
					{property: 'descp',label: '描述',sortable: false}
				],
				queryData:{'jsonLog':$.toJSON({'url':url,'user_name':user_name,'start':start,'end':end})},
				server:true,
		        stretchHeight: true
		    }),
		    grid=$('#logGrid').datagrid({
					dataSource: dsLogs,
					itemsText:" 条 "
				});

         //初始化日期组件
         $("#start").datetimepicker({format: 'yyyy-mm-dd hh:ii', forceParse: true});
         $("#end").datetimepicker({format: 'yyyy-mm-dd hh:ii', forceParse: true});


         $("#submit").click(function(){
             //重新准备查询条件
             var formData=$("#logquery").serializeJson();
             formData.start=formData.start?new Date(formData.start).getTime():'';
             formData.end=formData.end?new Date(formData.end).getTime():'';
             //将查询条件提交给dataSource
        	 dsLogs.setQueryData({'jsonLog':$.toJSON(formData)});
        	 //执行查询
        	 grid.reload();
         });
         
      });
			
   </script>