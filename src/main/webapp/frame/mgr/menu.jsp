<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
 <div class="page-header">
            <h3>菜单管理</h3>
  </div>
<table id="menuGrid" class="table table-bordered datagrid">
			<thead>
			<tr>
				<th>
					<div class="datagrid-header-left">
						 <button class="btn btn-success" id="add">添加<span id="root">ROOT</span>的子菜单</button>
						 <button class="btn btn-danger" id="del">删除</button>
					</div>
					<div class="datagrid-header-right">
					   <form class="form-inline" id="menuQuery">
						  <input type="text" name="uri" class="input-small" placeholder="资源位置">
						  <input type="text" name="menu_name" class="input-small" placeholder="菜单名称 ">
						  <button type="button" id="query" class="btn">查询</button>
						  <button type="reset" class="btn">重置</button>
					   </form>
					</div>
				</th>
			</tr>
			</thead>
	
			<tfoot>
			<tr>
				<th>
					<div class="datagrid-footer-left" style="display:none;">
						<div class="grid-controls">
							<span>
								<span class="grid-start"></span> -
								<span class="grid-end"></span> of
								<span class="grid-count"></span>
							</span>
							<div class="select grid-pagesize" data-resize="auto">
								<button data-toggle="dropdown" class="btn dropdown-toggle">
									<span class="dropdown-label"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li data-value="5" data-selected="true"><a href="#">5</a></li>
									<li data-value="10"><a href="#">10</a></li>
									<li data-value="20"><a href="#">20</a></li>
									<li data-value="50"><a href="#">50</a></li>
									<li data-value="100"><a href="#">100</a></li>
								</ul>
							</div>
							<span>每页</span>
						</div>
					</div>
					<div class="datagrid-footer-right" style="display:none;">
						<div class="grid-pager">
							<button type="button" class="btn grid-prevpage"><i class="icon-chevron-left"></i></button>
							<span>第</span>
	
							<div class="input-append dropdown combobox">
								<input class="span1" style="width: 49px;" type="text">
								<button class="btn" data-toggle="dropdown"><i class="caret"></i></button>
								<ul class="dropdown-menu"></ul>
							</div>
							<span>页 &nbsp;&nbsp;共 <span class="grid-pages"></span>页</span>
							<button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
						</div>
					</div>
				</th>
			</tr>
			</tfoot>
	
		</table>
		
       <div id="menu_dialog" style="display: none;" title="新增一个菜单">
        <form class="form-horizontal" id="menuinfo">
             <input type="hidden" id="parent_menu_id" name="parent_menu_id" value="-1">
		     <div class="control-group">
		        <label class="control-label" for="menu_name">菜单名称</label>
			    <div class="controls">
			      <input type="text" id="menu_name" name="menu_name" placeholder="菜单名称">
			    </div>
		     </div>
		      <div class="control-group">
		        <label class="control-label" for="menu_order">菜单顺序</label>
			    <div class="controls">
			      <input type="text" id="menu_order" name="menu_order" placeholder="菜单顺序">
			    </div>
		     </div>
		      <div class="control-group">
		        <label class="control-label" for="uri">资源位置</label>
			    <div class="controls">
			      <input type="text" id="uri" name="uri" placeholder="资源位置">
			    </div>
		     </div>
		</form>
    </div>
    
   <script>
   
      jQuery(function($){
         var formData=$("#menuQuery").serializeJson(),
             dsMenus=new AjaxDataSource({
	            url: "mgr/menus.do",  // values from Django view 
		        columns: [
					{property: 'menu_name',label: '菜单名称',sortable: true},
					{property: 'pmenu_name',label: '父菜单名称',sortable: true},
					{property: 'menu_order',label: '菜单顺序',sortable: true},
					{property: 'uri',label: '资源路径',sortable: true}
				],
				queryData:{'jsonMenu':$.toJSON(formData)},
				server:true,
		        stretchHeight: true
		    }),url="",
		    grid=$('#menuGrid').datagrid({
					dataSource: dsMenus,
					itemsText:" 条 ",
					itemSelected:function(){
				        //选中记录时炒作
				        var row=dsMenus.getSelectedRow(),name=row.menu_name;
				        $("#root").text(name);
				        $("#parent_menu_id").val(row.menu_id);
				    },
				    itemDeselected:function(){
				    	$("#root").text("ROOT");
				    	$("#parent_menu_id").val("-1");
					}
				});
			
			// Dialog
			
		    $("#menu_dialog").dialog({
		        autoOpen: false,
		        width: 600,
		        buttons: {
		            "Ok": function () {
		                $.post(url,$("#menuinfo").serialize(),function(){
		                    grid.reload();
						},'json');
		                $(this).dialog("close");
		                
		            },
		            "Cancel": function () {
		                $(this).dialog("close");
		            }
		        }
		    });
		    
			$("#add").click(function(){
			   $("#menuinfo").resetForm();
			   url="mgr/addMenu.do";
			   $("#menu_dialog").dialog('open');
                return false;
			});
			
			$("#del").click(function(){
			  var data=dsMenus.getSelectedRow();
			  if(data){
			    $.post("mgr/delMenu.do",{"menu_id":data.menu_id},function(){
				     grid.reload();
				},'json');
			  }else{
				  f$.notice("FSI0000");
			  }
			});

			$("#query").click(function(){
				 //重新准备查询条件
	             var formData=$("#menuQuery").serializeJson();
	             //将查询条件提交给dataSource
	        	 dsMenus.setQueryData({'jsonMenu':$.toJSON(formData)});
	        	 //执行查询
	        	 grid.reload();
			});
			
      });
			
   </script>