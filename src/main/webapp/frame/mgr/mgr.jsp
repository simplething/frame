<%@ page language="java" import="java.util.*" pageEncoding="utf-8" contentType="text/html; charset=utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">     
    <title>系统管理</title>
    <link href="frame/css/fuelux.css" rel="stylesheet">
    <link href="frame/css/frame.css" rel="stylesheet">
    
    <link href="frame/css/jquery.ui.core.css" rel="stylesheet"/>  <!-- button required -->
    <link href="frame/css/jquery.ui.theme.css" rel="stylesheet"/> <!-- button required -->
    <link href="frame/css/jquery.ui.button.css" rel="stylesheet"/><!-- button required -->
    
    <link href="frame/css/jquery.ui.resizable.css" rel="stylesheet"> <!-- dialog required -->
    <link href="frame/css/jquery.ui.dialog.css" rel="stylesheet">
    
    <link rel="stylesheet" href="frame/css/datetimepicker.css">
	
  </head>
  
  <body>
   
   <div class="container-fluid">
		<div class="row-fluid">
			<div class="span2">
			  <div class="well sidebar-nav affix">
				<ul class="nav nav-list">
					<li class="nav-header">
						<h3>系统控制</h3>
					</li>
					<li class="active">
						<a id="role">用户角色</a>
					</li>
					<li>
						<a id="group">用户分组</a>
					</li>
					<li>
						<a id="user">用户管理</a>
					</li>
					<li>
						<a id="resource">资源管理</a>
					</li>
					<li>
						<a id="code">字典管理</a>
					</li>
					<li>
						<a id="menu">菜单管理</a>
					</li>
					<li>
						<a id="log">日志管理</a>
					</li>
					<li class="divider"></li>
					<li>
						<a id="druid" href="druid/api.html" target="_black">druid监控</a>
					</li>
					<li>
						<a id="about">关于Frame</a>
					</li>
					<li>
						<a id="logout" href="gate/logout.do" target="_self">Logout</a>
					</li>
				</ul>
			  </div>
			</div>
			<div class="span10" id="otherpage">

		   </div>
		</div>
	</div>
	
	<script type="text/javascript" src="frame/lib/jquery-1.9.0.min.js"></script>
	<script type="text/javascript" src="frame/lib/bootstrap.min.js"></script>
	<script type="text/javascript" src="frame/js/jquery.fuelux.datagrid.js"></script>
	<script type="text/javascript" src="frame/js/datagridDS.js"></script>
	<script type="text/javascript" src="frame/js/jquery.fuelux.checkbox.js"></script>
	<script type="text/javascript" src="frame/js/jquery.fuelux.radio.js"></script>
	<script type="text/javascript" src="frame/js/jquery.fuelux.select.js"></script>
	<script type="text/javascript" src="frame/js/jquery.ui.core.js"></script>
	<script type="text/javascript" src="frame/js/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="frame/js/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="frame/js/jquery.ui.draggable.js"></script>
	<script type="text/javascript" src="frame/js/jquery.ui.position.js"></script>
	<script type="text/javascript" src="frame/js/jquery.ui.resizable.js"></script>
	<script type="text/javascript" src="frame/js/jquery.ui.button.js"></script>
	<script type="text/javascript" src="frame/js/jquery.ui.dialog.js"></script>
	
	<script type="text/javascript" src="frame/js/jquery.ui.notice.js"></script>
	<script type="text/javascript" src="frame/js/frame.message.js"></script>
	<script type="text/javascript" src="frame/js/frame.js"></script>
	<script type="text/javascript" src="frame/js/frame.codeName.js"></script>
	
	<script type="text/javascript">

	$("#otherpage").load("frame/mgr/role.jsp");
	    
		$("#role").click(function(){
		  $("#otherpage").load("frame/mgr/role.jsp");
		});
		
		$("#group").click(function(){
		   $("#otherpage").load("frame/mgr/group.jsp");
		});
		
		$("#resource").click(function(){
		   $("#otherpage").load("frame/mgr/resource.jsp");
		});
		
		$("#user").click(function(){
		  $("#otherpage").load("frame/mgr/user.jsp");
		});
		
		$("#code").click(function(){
			 $("#otherpage").load("frame/mgr/codemgr.jsp");
		});
		
		$("#menu").click(function(){
			$("#otherpage").load("frame/mgr/menu.jsp");
		});
		$("#log").click(function(){
			//动态加载脚本
			$.getScript("frame/js/bootstrap-datetimepicker.min.js");
			$.getScript("frame/js/locales4Date/bootstrap-datetimepicker.zh-CN.js",function(){
				$("#otherpage").load("frame/mgr/log.jsp");  
			});
		});
		$("#about").click(function(){
		   $("#otherpage").load("frame/about.jsp");
		});
		//$("#druid").click(function(){
			// $("#otherpage").load("druid/api.html");
			
	    //});
		
		$(".nav-list >li >a").parent().click(function(){
		   $(this).siblings().not(".divider").not(".nav-header").removeAttr("class");
		   $(this).attr("class","active");
		});
	</script>
   
	
  </body>
</html>
