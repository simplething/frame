<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
   <div class="page-header">
            <h3>资源管理</h3>
      </div>
		<table id="resourceGrid" class="table table-bordered datagrid">
			<thead>
			<tr>
				<th colspan="8">
					<div class="datagrid-header-left">
					    
						 <button class="btn btn-success" id="add">新增</button>
						 <button class="btn btn-warning" id="edit">修改</button>
						 <button class="btn btn-danger" id="del">删除</button>
					</div>
					<div class="datagrid-header-right">
					   <form class="form-inline" id="resourceQuery">
						  <input type="text" name="uri" class="input-small" placeholder="资源位置">
						  <input type="text" name="f_name" class="input-small" placeholder="资源名称 ">
						  <label class="checkbox">
						    <input type="checkbox" name="valid_flag"> 是否有效
						  </label>
						  <button type="button" id="query" class="btn">查询</button>
						  <button type="reset" class="btn">重置</button>
					   </form>
					</div>
				</th>
			</tr>
			</thead>
	
			<tfoot>
			<tr>
				<th>
					<div class="datagrid-footer-left" style="display:none;">
						<div class="grid-controls">
							<span>
								<span class="grid-start"></span> -
								<span class="grid-end"></span> of
								<span class="grid-count"></span>
							</span>
							<div class="select grid-pagesize" data-resize="auto">
								<button data-toggle="dropdown" class="btn dropdown-toggle">
									<span class="dropdown-label"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li data-value="10" data-selected="true"><a href="#">10</a></li>
									<li data-value="50"><a href="#">50</a></li>
									<li data-value="100"><a href="#">100</a></li>
								</ul>
							</div>
							<span>每页</span>
						</div>
					</div>
					<div class="datagrid-footer-right" style="display:none;">
						<div class="grid-pager">
							<button type="button" class="btn grid-prevpage"><i class="icon-chevron-left"></i></button>
							<span>第</span>
	
							<div class="input-append dropdown combobox">
								<input class="span1" style="width: 49px;" type="text">
								<button class="btn" data-toggle="dropdown"><i class="caret"></i></button>
								<ul class="dropdown-menu"></ul>
							</div>
							<span>页 &nbsp;&nbsp;共 <span class="grid-pages"></span>页</span>
							<button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
						</div>
					</div>
				</th>
			</tr>
			</tfoot>
	
		</table>
<div id="resource_dialog" style="display: none;" title="新增一个资源">
        <form class="form-horizontal" id=resource_info>
             <div class="control-group">
		        <label class="control-label" for="uri">资源位置</label>
			    <div class="controls">
			      <input type="text" id="uri" name="uri" placeholder="uri must be with / ahead">
    		    </div>
		     </div>
		     <div class="control-group">
		        <label class="control-label" for="f_name">资源名称</label>
			    <div class="controls">
			      <input type="text" id="f_name" name="f_name" placeholder="资源名称">
			    </div>
		     </div>
		     <div class="control-group">
		        <div class="controls">
		          <label class="checkbox checkbox-custom">
		            <input type="checkbox" id="valid_flag_s" name="valid_flag" checked="checked"/>
		            <i class="checkbox"></i>是否有效
		          </label>
		        </div>
		    </div>
		    <div class="control-group">
		        <label class="control-label" for="group_name">资源描述</label>
			    <div class="controls">
			      <textarea rows="4" id="description" name="description"></textarea>
			    </div>
		     </div>
		</form>
    </div>
    <div id="permitRole" style="display: none;"></div>
    <div id="permitGroup" style="display: none;"></div>
   <script>
	 jQuery(function($){
		  var formData=$("#resourceQuery").serializeJson(),
		      dsResource=new AjaxDataSource({
		        url: "mgr/resources.do",
		        columns: [
					{property: 'uri', label: '资源位置', sortable: true},
					{property: 'f_name', label: '资源名称',sortable: false},
					{property: 'valid_flag',label: '是否有效',sortable: true,render:function(r){
					   return f$.getCodeInfo("yn",r.valid_flag);
					}},
					{property: 'description',label: '资源描述',sortable: false},
					{property: '',label: '操作',render:function(r){
					   return "<a class='a-hand' atype='role' value='"+r.uri+"'>授权到角色</a> | <a class='a-hand' atype='group' value='"+r.uri+"'>授权到分组</a>";
					}}
				],
				queryData:{'query':$.toJSON(formData)},
				server:true,
				serverSorting:true,
		        stretchHeight: true
		    }),rdialog=null,gdialog=null;
		var rsGrid=$('#resourceGrid').datagrid({
				dataSource: dsResource,
				itemsText:" 条 ",
				sortDirection:"asc",
				stretchHeight: false
			});
			
			// Dialog
		    $("#resource_dialog").dialog({
		        autoOpen: false,
		        width: 600,
		        buttons: {
		            "Ok":  function () {
		                $("#uri").removeAttr("disabled");
		               // console.log($("#resource_info").serializeArray());
		                $.post(url,$("#resource_info").serialize(),function(){
		                	rsGrid.reload();
						},'json');
		                $(this).dialog("close");
		            },
		            "Cancel": function () {
		                $(this).dialog("close");
		            }
		        }
		    });
		    
			$("#add").click(function(){
			    $("#resource_info").resetForm();
			    $("#uri").removeAttr("disabled");
			    url="mgr/addResources.do";
			   $("#resource_dialog").dialog('open');
              return false;
			});
			$("#edit").click(function(){
			  var data=dsResource.getSelectedRow();
			  if(data){
			      $("#uri").val(data.uri).attr("disabled",true);
				  $("#f_name").val(data.f_name);
				  $("#description").val(data.description);
				  
				  if(($("#valid_flag_s:checked").length==1 && data.valid_flag=='N')||
				     ($("#valid_flag_s:checked").length==0 && data.valid_flag=='Y') ){
				    $("#valid_flag_s").checkbox('toggle');
				  }
				  url="mgr/editResources.do";
				  $("#resource_dialog").dialog('open');
			  }else{
				  f$.notice("FSI0000");
			  }
                return false;
			});
			$("#del").click(function(){
			  var data=dsResource.getSelectedRow();
			  if(data){
			    $.post("mgr/delResources.do",{"resource_id":data.uri},function(){
			    	rsGrid.reload();
				},'json');
			  }else{
				  f$.notice("FSI0000");
			  }
			});
			
			$("table").on( "click","[atype=role]", function() {
			   
			   if(rdialog && $("body").data("u")==$(this).attr("value")){
			       rdialog.dialog('open');
			   }else{
			     $("body").data("u",$(this).attr("value"));//将这个路径放在数据共享区
			     $("body").data("roleUrl","mgr/getPermitRoles.do");
			     $("#permitRole").load("frame/mgr/rolePermit.jsp",function(){
				     rdialog=$("#permitRole").dialog({
				        autoOpen: false,
				        width: 600,
				        title: "将资源授权给角色",
				        buttons: {
				            "授权/释权": function () {
				              var rows=$("body").data("dsRoles").getSelectedRows(),arg={};
				              arg.uri=dsResource.getSelectedRow().uri;
				              arg.roles=rows;
                             $.ajax({type:'POST',
							    url: "mgr/updatePermitRoles.do", 
								data:$.toJSON(arg),
							    dataType:'json',
							    contentType:"application/json",
							    success: function(){
								    rdialog=null;//如果操作成功，将这个对话框的引用置为null,以便下次重新加载
								}
						     });
				             $(this).dialog("close");
				            },
				            "取消": function () {
				                $(this).dialog("close");
				            }
				        }
				    }).dialog('open');;  
							   
				 });
			   }
			   
			} ); 
			$("table").on( "click","[atype=group]", function() {
			   if(gdialog && $("body").data("u")==$(this).attr("value")){
			       gdialog.dialog('open');
			   }else{
			     $("body").data("u",$(this).attr("value"));
			     $("body").data("groupUrl","mgr/getPermitGroups.do");
			     $("#permitGroup").load("frame/mgr/groupPermit.jsp",function(){
				     gdialog=$("#permitGroup").dialog({
				        autoOpen: false,
				        width: 600,
				        title: "将资源授权给分组",
				        buttons: {
				            "授权/释权": function () {
				              var rows=$("body").data("dsGroups").getSelectedRows(),arg={};
				              arg.uri=dsResource.getSelectedRow().uri;
				              arg.groups=rows;
                             $.ajax({type:'POST',
							    url: "mgr/updatePermitGroups.do", 
								data:$.toJSON(arg),
							    dataType:'json',
							    contentType:"application/json",
							    success: function(){
								    gdialog=null;//如果操作成功，将这个对话框的引用置为null,以便下次重新加载
								}
						     });
				             $(this).dialog("close");
				                
				            },
				            "取消": function () {
				                $(this).dialog("close");
				            }
				        }
				    }).dialog('open');;  
							   
				 });
			   }
			   
			} ); 
			$(".nav-list >li >a").parent().click(function(){
			   $(this).siblings().not(".divider").not(".nav-header").removeAttr("class");
			   $(this).attr("class","active");
			});
			
			$("#valid_flag_s").checkbox();

			$("#query").click(function(){
				 //重新准备查询条件
	             var formData=$("#resourceQuery").serializeJson();
	             //将查询条件提交给dataSource
	        	 dsResource.setQueryData({'query':$.toJSON(formData)});
	        	 //执行查询
	        	 rsGrid.reload();
			});
			
		});	
		
   </script>
