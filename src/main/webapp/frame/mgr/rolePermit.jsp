<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
		<table id="roleGrid" class="table table-bordered datagrid">
			<thead>
			</thead>
			<tfoot>
			<tr>
				<th>
					<div class="datagrid-footer-left" style="display:none;">
						<div class="grid-controls">
							<span>
								<span class="grid-start"></span> -
								<span class="grid-end"></span> of
								<span class="grid-count"></span>
							</span>
							<div class="select grid-pagesize" data-resize="auto">
								<button data-toggle="dropdown" class="btn dropdown-toggle">
									<span class="dropdown-label"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li data-value="5" data-selected="true"><a href="#">5</a></li>
									<li data-value="10"><a href="#">10</a></li>
									<li data-value="20"><a href="#">20</a></li>
									<li data-value="50"><a href="#">50</a></li>
									<li data-value="100"><a href="#">100</a></li>
								</ul>
							</div>
							<span>每页</span>
						</div>
					</div>
					<div class="datagrid-footer-right" style="display:none;">
						<div class="grid-pager">
							<button type="button" class="btn grid-prevpage"><i class="icon-chevron-left"></i></button>
							<span>第</span>
	
							<div class="input-append dropdown combobox">
								<input class="span1" style="width: 49px;" type="text">
								<button class="btn" data-toggle="dropdown"><i class="caret"></i></button>
								<ul class="dropdown-menu"></ul>
							</div>
							<span>页 &nbsp;&nbsp;共 <span class="grid-pages"></span>页</span>
							<button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
						</div>
					</div>
				</th>
			</tr>
			</tfoot>
		</table>
   <script>
      jQuery(function($){
         var dsRoles=new AjaxDataSource({
	            url: $("body").data("roleUrl"),
	            queryData:{"u":$("body").data("u")},
		        columns: [
					{property: 'role_name',label: '角色名称',sortable: true},
					{property: 'permit',label: '是否被授权',sortable: false,render:function(r){
					   return r.permit=='Y'?'是':'否';
					}}
				],
				server:true,
		        stretchHeight: true
		    });
		    $('#roleGrid').datagrid({
					dataSource: dsRoles,
					itemsText:" 条 ",
					selectedType:"checkbox"
			});
			$("body").data("dsRoles",dsRoles);//将当前DataSource绑定到body元素上，供父页面使用
			
      });
			
   </script>