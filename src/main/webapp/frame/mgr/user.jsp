<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
	<div class="page-header">
            <h3>用户管理</h3>
      </div>
		<table id="userGrid" class="table table-bordered datagrid ">
			<thead>
			<tr>
				<th>
					<div class="datagrid-header-left">
						 <button class="btn btn-success" id="add">新增</button>
						 <button class="btn btn-warning" id="edit">修改</button>
						 <button class="btn btn-danger" id="del">删除</button>
					</div>
				</th>
			</tr>
			</thead>
	
			<tfoot>
			<tr>
				<th>
					<div class="datagrid-footer-left" style="display:none;">
						<div class="grid-controls">
							<span>
								<span class="grid-start"></span> -
								<span class="grid-end"></span> of
								<span class="grid-count"></span>
							</span>
							<div class="select grid-pagesize" data-resize="auto">
								<button data-toggle="dropdown" class="btn dropdown-toggle">
									<span class="dropdown-label"></span>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li data-value="5" data-selected="true"><a href="#">5</a></li>
									<li data-value="10"><a href="#">10</a></li>
									<li data-value="20"><a href="#">20</a></li>
									<li data-value="50"><a href="#">50</a></li>
									<li data-value="100"><a href="#">100</a></li>
								</ul>
							</div>
							<span>每页</span>
						</div>
					</div>
					<div class="datagrid-footer-right" style="display:none;">
						<div class="grid-pager">
							<button type="button" class="btn grid-prevpage"><i class="icon-chevron-left"></i></button>
							<span>第</span>
	
							<div class="input-append dropdown combobox">
								<input class="span1" style="width: 49px;" type="text">
								<button class="btn" data-toggle="dropdown"><i class="caret"></i></button>
								<ul class="dropdown-menu"></ul>
							</div>
							<span>页 &nbsp;&nbsp;共 <span class="grid-pages"></span>页</span>
							<button type="button" class="btn grid-nextpage"><i class="icon-chevron-right"></i></button>
						</div>
					</div>
				</th>
			</tr>
			</tfoot>
	
		</table>
<div id="user_dialog" style="display: none;" title="新增一个用户">
        <form class="form-horizontal" id=user_info>
             <div class="control-group">
		        <label class="control-label" for="user_id">用户ID</label>
			    <div class="controls">
			      <input type="text" id="user_id" name="user_id" placeholder="user_id is the ID you used to login">
    		    </div>
		     </div>
		     <div class="control-group">
		        <label class="control-label" for="user_name">用户名称</label>
			    <div class="controls">
			      <input type="text" id="user_name" name="user_name" placeholder="用户名称">
			    </div>
		     </div>
		    <div class="control-group">
			    <label class="control-label" for="password">密码</label>
			    <div class="controls">
			      <input type="password" id="password" name="password" placeholder="Password">
			    </div>
			</div>
			<div class="control-group">
			    <label class="control-label" for="password2">确认密码</label>
			    <div class="controls">
			      <input type="password" id="password2" placeholder="请再输一遍密码">
			    </div>
			</div>
		</form>
    </div>
    <div id="permitRole" style="display: none;"></div>
    <div id="permitGroup" style="display: none;"></div>
   <script>
	 jQuery(function($){
		  var dsuser=new AjaxDataSource({
		        url: "mgr/users.do",
		        columns: [
					{property: 'user_id', label: '用户ID', sortable: true},
					{property: 'user_name', label: '用户名称',sortable: false},
					{property: 'create_date',label: '创建时间',sortable: true},
					{property: '',label: '操作',render:function(r){
					   return "<a class='a-hand' atype='role' value='"+r.user_id+"'>绑定到角色</a> | <a class='a-hand' atype='group' value='"+r.user_id+"'>绑定到分组</a>";
					}}
				],
				server:true,
		        stretchHeight: true
		    }),rdialog=null,gdialog=null;
		var grid=$('#userGrid').datagrid({
				dataSource: dsuser,
				itemsText:" 条 "
			});
			
			// Dialog
		    $("#user_dialog").dialog({
		        autoOpen: false,
		        width: 600,
		        buttons: {
		            "Ok":  function () {
		                $("#user_id").removeAttr("disabled");
		               // console.log($("#user_info").serializeArray());
		                $.post(url,$("#user_info").serialize(),function(){
		                    grid.reload();
						},'json');
		                $(this).dialog("close");
		            },
		            "Cancel": function () {
		                $(this).dialog("close");
		            }
		        }
		    });
		    
			$("#add").click(function(){
			    $("#user_info").resetForm();
			    $("#user_id").removeAttr("disabled");
			    url="mgr/addUser.do";
			   $("#user_dialog").dialog('open');
              return false;
			});
			$("#edit").click(function(){
			  var data=dsuser.getSelectedRow();
			  if(data){
			      $("#user_id").val(data.user_id).attr("disabled",true);
				  $("#user_name").val(data.user_name);
				  $("#description").val(data.description);
				  
				  if(($("#valid_flag_s:checked").length==1 && data.valid_flag=='N')||
				     ($("#valid_flag_s:checked").length==0 && data.valid_flag=='Y') ){
				    $("#valid_flag_s").checkbox('toggle');
				  }
				 $("#userLevel input[name='power_level']").val([data.power_level]);
				 $("#userLevel").buttonset("refresh");
				  
				  url="mgr/editUser.do";
				  $("#user_dialog").dialog('open');
			  }else{
				  f$.notice("FSI0000");
			  }
                return false;
			});
			$("#del").click(function(){
			  var data=dsuser.getSelectedRow();
			  if(data){
			    $.post("mgr/delUser.do",{"user_id":data.user_id},function(){
				     grid.reload();
				},'json');
			  }else{
				  f$.notice("FSI0000");
			  }
			});
			
			$("table").on( "click","[atype=role]", function() {
			  
			   if(rdialog && $("body").data("u")==$(this).attr("value")){
			       rdialog.dialog('open');
			   }else{
			     $("body").data("u",$(this).attr("value"));//将这个路径放在数据共享区
			     $("body").data("roleUrl","mgr/getUserRoles.do");
			     $("#permitRole").load("frame/mgr/rolePermit.jsp",function(){
				     rdialog=$("#permitRole").dialog({
				        autoOpen: false,
				        width: 600,
				        title: "将用户绑定到角色",
				        buttons: {
				            "绑定/解绑": function () {
				              var rows=$("body").data("dsRoles").getSelectedRows(),arg={};
				              arg.user_id=dsuser.getSelectedRow().user_id;
				              arg.roles=rows;
                             $.ajax({type:'POST',
							    url: "mgr/updateUserRoles.do", 
								data:$.toJSON(arg),
							    dataType:'json',
							    contentType:"application/json",
							    success: function(){
								    rdialog=null;//如果操作成功，将这个对话框的引用置为null,以便下次重新加载
								}
						     });
				             $(this).dialog("close");
				            },
				            "取消": function () {
				                $(this).dialog("close");
				            }
				        }
				    }).dialog('open');;  
							   
				 });
			   }
			   
			} ); 
			$("table").on( "click","[atype=group]", function() {
			   if(gdialog && $("body").data("u")==$(this).attr("value")){
			       gdialog.dialog('open');
			   }else{
			     $("body").data("u",$(this).attr("value"));
			     $("body").data("groupUrl","mgr/getUserGroups.do");
			     $("#permitGroup").load("frame/mgr/groupPermit.jsp",function(){
				     gdialog=$("#permitGroup").dialog({
				        autoOpen: false,
				        width: 600,
				        title: "将用户绑定到分组",
				        buttons: {
				            "绑定/解绑": function () {
				              var rows=$("body").data("dsGroups").getSelectedRows(),arg={};
				              arg.user_id=dsuser.getSelectedRow().user_id;
				              arg.groups=rows;
                             $.ajax({type:'POST',
							    url: "mgr/updateUserGroups.do", 
								data:$.toJSON(arg),
							    dataType:'json',
							    contentType:"application/json",
							    success: function(){
								    gdialog=null;//如果操作成功，将这个对话框的引用置为null,以便下次重新加载
								}
						     });
				             $(this).dialog("close");
				                
				            },
				            "取消": function () {
				                $(this).dialog("close");
				            }
				        }
				    }).dialog('open');;  
							   
				 });
			   }
			   
			} ); 
			
			$(".nav-list >li >a").parent().click(function(){
			   $(this).siblings().not(".divider").not(".nav-header").removeAttr("class");
			   $(this).attr("class","active");
			});
			
		});	
		
   </script>
