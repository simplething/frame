<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en">
  <head> 
    <base href="<%=basePath%>">    
    <meta charset="utf-8">
    <title>Sign in &middot; Twitter Bootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="frame/css/fuelux.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
  </head>

  <body>

    <div class="container form-signin">
     <form class="form-signin" id="user">
        <h2 class="form-signin-heading">管理员登录</h2>
        <input type="text" class="input-block-level" placeholder="用户名" name="user_id">
        <input type="password" class="input-block-level" placeholder="密码" name="password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </form>
      <button class="btn btn-large btn-primary" id="login">管理员登录</button>
    </div> <!-- /container -->

    
  </body>
  <script type="text/javascript" src="frame/lib/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="frame/lib/bootstrap.min.js"></script>
    <script type="text/javascript">
	    $("#login").click(function(){
	      //  console.log($("#user").serialize());
	        $.post("gate/mgrlogin.do",$("#user").serialize(),function(r){
				window.location.href=r;
			});
	    });
    </script>
</html>
